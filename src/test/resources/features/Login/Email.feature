@Login @Email
@SetupDriver
Feature: Login
  As a user not logged
  I want to login the web
  In order to get full access on the app


  Scenario: User makes log in with nonexisting email address
    Given I am at the input page
    When I go to the login page
    And I make login with "invalidLogin@starzplayarabia.com" and "tester01."
    Then the invalidLogin popup is displayed

  Scenario: User makes log in with wrong format email address
    Given I am at the input page
    When I go to the login page
    And I make login with "invalidLogin" and "tester01."
    Then I am at the login page

  Scenario: User makes log in with no password
  Scenario: User makes log in with short password
  Scenario: User makes log in with wrong password

  @mikel
  Scenario: User makes log in with correct email and correct password
    Given I am at the input page
    When I go to the login page
    And I make login with "aue@playco.com" and "tester01."
    Then I am at the home page

  Scenario: User retrieves password reset link on email address
  Scenario: User uses password reset link to reset password