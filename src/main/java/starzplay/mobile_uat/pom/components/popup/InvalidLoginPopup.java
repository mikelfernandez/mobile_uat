package starzplay.mobile_uat.pom.components.popup;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import starzplay.mobile_uat.pom.BasePage;
import starzplay.mobile_uat.utility.Constant;

public class InvalidLoginPopup extends BasePage {

    //Webelement popup invalid login
    @AndroidFindBy(id = "android:id/message")
    private WebElement webEl_invalidLoginPopUp;

    //Button OK
    @FindBy(id = "android:id/button2")
    private WebElement btn_OK;

    public void acceptInvalidLogin(){
        new WebDriverWait(driver, Constant.WAIT_SHORT).until(ExpectedConditions.elementToBeClickable(btn_OK));
        btn_OK.click();
    }

    public boolean invalidLoginIsDisplayed(){
        new WebDriverWait(driver, Constant.WAIT_SHORT).until(ExpectedConditions.elementToBeClickable(webEl_invalidLoginPopUp));
        return webEl_invalidLoginPopUp.isDisplayed();
    }



}