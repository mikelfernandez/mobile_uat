package starzplay.mobile_uat.pom.components.popup;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.BasePage;

public class DeactivateUserPopup extends BasePage {

    //Relative Layout
    @AndroidFindBy(id = "com.parsifal.starz:id/dialog_login_required_message")
    public WebElement webEl_relLayout;

    //Button Landing
    @AndroidFindBy(id = "com.parsifal.starz:id/message_prospect")
    public WebElement txt_message;

    //Button close popup
    @AndroidFindBy(id = "com.parsifal.starz:id/close_prospect")
    public WebElement btn_closePopup;


}