package starzplay.mobile_uat.pom.components.popup;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class LoginPopup extends BasePage {

    //Text login required
    @AndroidFindBy(id = "com.parsifal.starz:id/dialog_login_required_message")
    public WebElement txt_loginReq;

    //Button Log in
    @FindBy(id = "com.parsifal.starz:id/dialog_login_required_button")
    public WebElement btn_Login;

    //Button Landing
    @AndroidFindBy(id = "com.parsifal.starz:id/dialog_login_required_signup_button")
    public WebElement btn_Signup;

    //Button close popup
    @AndroidFindBy(id = "com.parsifal.starz:id/dialog_login_required_close")
    public WebElement btn_closePopup;


}