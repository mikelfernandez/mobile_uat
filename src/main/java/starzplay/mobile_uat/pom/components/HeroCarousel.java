package starzplay.mobile_uat.pom.components;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class HeroCarousel extends BasePage{
    //Image hero
    @AndroidFindBy(id = "com.parsifal.starz:id/pageritem_hero_image")
    private WebElement img_Hero;

    //Button add watchlist
    @AndroidFindBy(id = "com.parsifal.starz:id/module_hero_watchlist_btn")
    private WebElement btn_AddWatchlist;

    //Button add watchlist
    @AndroidFindBy(id = "com.parsifal.starz:id/module_hero_watchlist_btn")
    private List<WebElement> list_btn_AddWatchlist;

    //Button Watch now
    @AndroidFindBy(id = "com.parsifal.starz:id/module_hero_play_now_btn")
    private WebElement btn_WatchNow;

    //Button Watch trailer
    @AndroidFindBy(id = "com.parsifal.starz:id/module_hero_trailer_btn")
    private WebElement btn_WatchTrailer;

    public boolean addWatchListButtonIsDisplayed(){
        return list_btn_AddWatchlist.size() >0;

    }

    public boolean isLogged() {
        return addWatchListButtonIsDisplayed();
    }

}
