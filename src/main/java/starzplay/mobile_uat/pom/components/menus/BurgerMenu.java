package starzplay.mobile_uat.pom.components.menus;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.BasePage;

public class BurgerMenu extends BasePage {

    //Elem categories display
    @AndroidFindBy(id = "com.parsifal.starz:id/relativeLayoutMenu")
    public WebElement elem_Categories;

    //Button Login/Settings
    @AndroidFindBy(id = "com.parsifal.starz:id/tv_settings")
    public WebElement btn_Settings;

    //Button Series category
    @AndroidFindBy(xpath = "//android.widget.ListView[@resource-id='com.parsifal.starz:id/listViewLevel1']//android.widget.RelativeLayout[1]")
    public WebElement btn_Home;

    //Button Series category
    @AndroidFindBy(xpath = "//android.widget.ListView[@resource-id='com.parsifal.starz:id/listViewLevel1']//android.widget.RelativeLayout[2]")
    public WebElement btn_Series;

    //Button Movies category
    @AndroidFindBy(xpath = "//android.widget.ListView[@resource-id='com.parsifal.starz:id/listViewLevel1']//android.widget.RelativeLayout[3]")
    public WebElement btn_Movies;

    //Button Kids category
    @AndroidFindBy(xpath = "//android.widget.ListView[@resource-id='com.parsifal.starz:id/listViewLevel1']//android.widget.RelativeLayout[4]")
    public WebElement btn_Genres;

    //Button Kids category
    @AndroidFindBy(xpath = "//android.widget.ListView[@resource-id='com.parsifal.starz:id/listViewLevel1']//android.widget.RelativeLayout[5]")
    public WebElement btn_Kids;

    //Button Watchlist
    @AndroidFindBy(xpath = "//android.widget.ListView[@resource-id='com.parsifal.starz:id/listViewLevel1']//android.widget.RelativeLayout[6]")
    public WebElement btn_Watchlist;

    //Button Watchlist
    @AndroidFindBy(xpath = "//android.widget.ListView[@resource-id='com.parsifal.starz:id/listViewLevel1']//android.widget.RelativeLayout[6]")
    public WebElement btn_Help;

    //Button Watchlist
    @AndroidFindBy(id = "com.parsifal.starz:id/signup")
    public WebElement btn_Signup;


}