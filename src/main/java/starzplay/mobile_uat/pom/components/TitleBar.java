package starzplay.mobile_uat.pom.components;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class TitleBar extends BasePage {

    //Button Burger Menu
    @FindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.ImageButton")
    public WebElement btn_burgerMenu;

    //Button Burger Menu
    @FindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.ImageView")
    public WebElement img_StarzLogo;

    //Button Series
    @FindBy(id = "com.parsifal.starz:id/toolbar_title")
    public WebElement btn_titlePage;

    //Button Series
    @FindBy(id = "com.parsifal.starz:id/action_search")
    public WebElement btn_Search;

}
