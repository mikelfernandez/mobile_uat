package starzplay.mobile_uat.pom;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;

public class BasePage {


    public static RemoteWebDriver driver;

    public BasePage(){
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);

    }


}
