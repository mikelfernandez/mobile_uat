package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class SelectActionPage extends BasePage {


    //Text page name
    @FindBy(id = "android:id/alertTitle")
    public MobileElement txt_pageName;

    //Grid of actions to select
    @FindBy(id = "android:id/resolver_grid")
    public MobileElement mobElem_gridSelectAction;

    //Text user name
    @FindBy(id = "android:id/text1")
    public List<MobileElement> mobElem_selectActionsApps;

}