package starzplay.mobile_uat.pom.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import starzplay.mobile_uat.pom.BasePage;

public class ContentInformationPage extends BasePage {

    //Text page name
    @FindBy(id = "com.parsifal.starz:id/toolbar_title")
    public WebElement txt_PageName;

    //Button close view
    @FindBy(id = "com.parsifal.starz:id/toolbar_close_button")
    public WebElement btn_closeView;

    //WebElement hero
    @FindBy(id = "com.parsifal.starz:id/pageritem_hero_image")
    public WebElement webEl_Hero;

    //Button add watchlist
    @FindBy(id = "com.parsifal.starz:id/module_hero_watchlist_btn")
    public WebElement btn_addWatchlist;

    //Text title of the content
    @FindBy(id = "com.parsifal.starz:id/text_view_title")
    public WebElement txt_Title;

    //Button Watch now/Resume
    @FindBy(id = "com.parsifal.starz:id/module_hero_play_now_btn")
    public WebElement btn_watchNow;

    //Button watch now
    @FindBy(id = "com.parsifal.starz:id/module_hero_trailer_btn")
    public WebElement btn_watchTrailer;

    public boolean titleIsDisplayed(){
        return driver.findElements(By.id("com.parsifal.starz:id/text_view_title")).size() >0;

    }

    public boolean addWatchListButtonIsDisplayed(){
        return driver.findElements(By.id("com.parsifal.starz:id/module_hero_watchlist_btn")).size() >0;
    }

    public void waitForAddContentMessage(){
        new WebDriverWait(driver, 45).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.FrameLayout[@resource-id='android:id/content']/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.TextView")));
    }

}