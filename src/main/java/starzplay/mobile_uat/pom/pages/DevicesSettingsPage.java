package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class DevicesSettingsPage extends BasePage {

    //Text page name
    @FindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.TextView")
    public WebElement txt_pageTitle;

    //Devices List
    @FindBy(xpath = "//android.support.v7.widget.RecyclerView//android.widget.FrameLayout")
    public List<WebElement> list_devices;
}
