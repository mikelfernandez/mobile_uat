package starzplay.mobile_uat.pom.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class ParentalSettingsPage extends BasePage{

    //Text page name
    @FindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.TextView")
    private WebElement txt_pageTitle;

    //Parental list
    @FindBy(xpath = "//android.widget.LinearLayout[@resource-id='com.parsifal.starz:id/parentalList']//android.widget.FrameLayout")
    private List<WebElement> list_parental;

    public WebElement getG(){
        return list_parental.get(0);
    }

    public WebElement getPG(){
        return list_parental.get(1);
    }
    public WebElement get15age(){
        return list_parental.get(2);
    }
    public WebElement getR(){
        return list_parental.get(3);
    }


}
