package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class SettingsPage extends BasePage {

    //Tabs list
    @FindBy(className = "android.support.v7.app.ActionBar$Tab")
    public WebElement btn_Profiles;

    //Tabs list
    @FindBy(className = "android.support.v7.app.ActionBar$Tab")
    public List<WebElement> btn_Payments;

    //Tabs list
    @FindBy(className = "android.support.v7.app.ActionBar$Tab")
    public List<WebElement> btn_Devices;

    //Tabs list
    @FindBy(className = "android.support.v7.app.ActionBar$Tab")
    public List<WebElement> btn_ParControl;

    //Tabs list
    @FindBy(xpath = "//android.widget.TextView[@resource-id='com.parsifal.starz:id/tab_title']")
    public List<WebElement> list_tabs;


    /**
     * Profiles page
     */
    //Text page name
    @FindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.TextView")
    public WebElement txt_pageTitle;

    public WebElement getProfile(){
        return list_tabs.get(0);
    }
    public WebElement getPayments(){
        return list_tabs.get(1);
    }
    public WebElement getDevices(){
        return list_tabs.get(2);
    }
    public WebElement getParental(){
        return list_tabs.get(3);
    }

}