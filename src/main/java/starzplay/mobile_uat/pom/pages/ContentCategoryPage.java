package starzplay.mobile_uat.pom.pages;


import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import starzplay.mobile_uat.pom.BasePage;
import starzplay.mobile_uat.utility.CommonActions;

import java.util.List;

public class ContentCategoryPage extends BasePage {

    //Text page name
    @AndroidFindBy(id = "com.parsifal.starz:id/toolbar_title")
    public WebElement txt_PageName;

    //WebElement character list
    @AndroidFindBy(id = "com.parsifal.starz:id/tv_title")
    public List<WebElement> webElementList_characterList;

    //Image character list
    @AndroidFindBy(id = "com.parsifal.starz:id/iv_character_img")
    public WebElement img_characterList;

    //WebElement hero
    @AndroidFindBy(id = "com.parsifal.starz:id/pageritem_hero_image")
    public WebElement webEl_Hero;

    //Tab Recommended
    @AndroidFindBy(id = "com.parsifal.starz:id/module_catalogue_sort_btn_recommended")
    public WebElement tab_Recommended;

    //Tab Alphabetical
    @AndroidFindBy(id = "com.parsifal.starz:id/module_catalogue_sort_btn_alphabetical")
    public WebElement tab_Alphabetical;

    //Tab Last Added
    @AndroidFindBy(id = "com.parsifal.starz:id/module_catalogue_sort_btn_last_added")
    public WebElement tab_lastAdded;

    //WebElement movie list
    @AndroidFindBy(id = "com.parsifal.starz:id/lv_movie_list")
    public WebElement webEl_movieList;

    //WebElement movie list for tablet
    @AndroidFindBy(id = "com.parsifal.starz:id/module_catalogue_movie_grid")
    public WebElement webEl_movieList_tablets;

    //WebElement letter list
    @AndroidFindBy(id = "com.parsifal.starz:id/lv_letter_list")
    public WebElement webEl_letterList;

    //WebElement letter list
    @AndroidFindBy(id = "com.parsifal.starz:id/tv_letter")
    public List<WebElement> webElList_letterList;

    //WebElement letter list for tablet
    @AndroidFindBy(id = "com.parsifal.starz:id/item_letter_char")
    public List<WebElement> webElList_letterList_tablet;

    //WebElement letter list for tablet
    @AndroidFindBy(id = "com.parsifal.starz:id/module_catalogue_letters_container")
    public WebElement webEl_letterList_tablet;

    //Text movie title
    @AndroidFindBy(id = "com.parsifal.starz:id/tv_title")
    public WebElement txt_movieTitle;

    public void waitForMovies(){
        if (CommonActions.isTablet())
            new WebDriverWait(driver, 45).until(ExpectedConditions.presenceOfElementLocated(By.id("com.parsifal.starz:id/module_catalogue_movie_grid")));
        else
            new WebDriverWait(driver, 45).until(ExpectedConditions.presenceOfElementLocated(By.id("com.parsifal.starz:id/lv_movie_list")));
    }

    public void filterLetter(String letter) {
        if (CommonActions.isTablet()) {
            for (WebElement elem : webElList_letterList_tablet) {
                if (elem.getText().equals(letter))
                    elem.click();
            }
        } else {
            for (WebElement elem : webElList_letterList) {
                if (elem.getText().equals(letter.toLowerCase()))
                    elem.click();
            }
        }
    }

    public void swipeForTabs(){
        boolean heroImageisPresent = driver.findElements(By.id("com.parsifal.starz:id/pageritem_hero_image")).size() > 0;
        boolean viewPagerisPresent = driver.findElements(By.id("com.parsifal.starz:id/viewPager")).size() > 0;
        while (viewPagerisPresent || heroImageisPresent) {
            CommonActions.swipeUpShort();
            heroImageisPresent = driver.findElements(By.id("com.parsifal.starz:id/pageritem_hero_image")).size() > 0;
            viewPagerisPresent = driver.findElements(By.id("com.parsifal.starz:id/viewPager")).size() > 0;

        }
    }

    public void swipeRightCharacterList(){
        //driver.swipe(webElementList_characterList.get(2).getLocation().getX(), img_characterList.getLocation().getY(), webElementList_characterList.get(1).getLocation().getX(), img_characterList.getLocation().getY(), 1000);
    }

    public void clicRecomendedTab(){
        tab_Recommended.click();
    }

}