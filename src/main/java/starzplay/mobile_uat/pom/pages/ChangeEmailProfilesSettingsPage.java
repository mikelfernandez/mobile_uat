package starzplay.mobile_uat.pom.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class ChangeEmailProfilesSettingsPage extends BasePage {

    //Text page name
    @FindBy(id = "com.parsifal.starz:id/alertTitle")
    public WebElement txt_title;

    //Field current Email
    @FindBy(id = "com.parsifal.starz:id/editTextCurrentEmail")
    public WebElement fld_currentEmail;

    //Field New Email
    @FindBy(id = "com.parsifal.starz:id/editTextNewEmail")
    public WebElement fld_newEmail;

    //Field confirm New  Email
    @FindBy(id = "com.parsifal.starz:id/editTextConfirmNewEmail")
    public WebElement fld_confirmEmail;

    //Button ok
    @FindBy(id = "android:id/button1")
    public WebElement btn_OK;

    //Button cancel
    @FindBy(id = "android:id/button2")
    public WebElement btn_Cancel;


}
