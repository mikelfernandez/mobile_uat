package starzplay.mobile_uat.pom.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class RequestSuccessfulPage extends BasePage {


    //Text page name
    @FindBy(id = "android:id/message")
    public WebElement txt_title;

    //Button ok
    @FindBy(id = "android:id/button2")
    public WebElement btn_OK;




}
