package starzplay.mobile_uat.pom.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class ChangePassProfilesSettingsPage extends BasePage {

    //Text page name
    @FindBy(id = "com.parsifal.starz:id/alertTitle")
    public WebElement txt_title;

    //Field current Pass
    @FindBy(id = "com.parsifal.starz:id/editTextCurrentPassword")
    public WebElement fld_currentPass;

    //Field New Email
    @FindBy(id = "com.parsifal.starz:id/editTextNewPassword")
    public WebElement fld_newPass;

    //Field confirm New  Email
    @FindBy(id = "com.parsifal.starz:id/editTextConfirmNewPassword")
    public WebElement fld_confirmPass;

    //Button ok
    @FindBy(id = "android:id/button1")
    public WebElement btn_OK;

    //Button cancel
    @FindBy(id = "android:id/button2")
    public WebElement btn_Cancel;


}
