package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import starzplay.mobile_uat.pom.BasePage;
import starzplay.mobile_uat.utility.Constant;


public class InputPage extends BasePage{

    //Button Signup
    @AndroidFindBy(id = "com.parsifal.starz:id/buttonCreateAccount")
    public WebElement btn_Signup;

    //Button Login
    @AndroidFindBy(id = "com.parsifal.starz:id/buttonLogin")
    public WebElement btn_Login;

    //Button Explore the app
    @AndroidFindBy(id = "com.parsifal.starz:id/buttonBrowseApp")
    public WebElement btn_expApp;

    public boolean isSignupDisplayed(){
        new WebDriverWait(driver, Constant.WAIT_MEDIUM).until(ExpectedConditions.visibilityOf(btn_Signup));
        return btn_Signup.isDisplayed();

    }

    public boolean isLoginDisplayed(){
        new WebDriverWait(driver, Constant.WAIT_MEDIUM).until(ExpectedConditions.elementToBeClickable(btn_Login));
        return btn_Login.isDisplayed();

    }

    public boolean isExploreDisplayed(){
        new WebDriverWait(driver, Constant.WAIT_MEDIUM).until(ExpectedConditions.elementToBeClickable(btn_expApp));
        return btn_expApp.isDisplayed();

    }

    public void gotoLogin() {
        new WebDriverWait(driver, Constant.WAIT_MEDIUM).until(ExpectedConditions.visibilityOf(btn_Login));
        btn_Login.click();
    }
}
