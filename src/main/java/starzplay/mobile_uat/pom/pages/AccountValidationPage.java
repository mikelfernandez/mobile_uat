package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.BasePage;


public class AccountValidationPage extends BasePage {

    //Text page name
    @AndroidFindBy(id = "com.parsifal.starz:id/toolbar_left_arrow")
    public WebElement btn_Back;

    //Text page name
    @AndroidFindBy(id = "com.parsifal.starz:id/textViewValidation")
    public WebElement txt_pageName;

    //Image icon ticked
    @AndroidFindBy(id = "com.parsifal.starz:id/imageViewTickIcon")
    public WebElement img_iconTicked;

    //Text validate account
    @AndroidFindBy(id = "com.parsifal.starz:id/textViewValidateAccount")
    public WebElement txt_valAccount;

    //Combo select country
    @AndroidFindBy(id = "com.parsifal.starz:id/spinnerMobileNumber")
    public WebElement cmb_countrySelector;

    //Field mobile Number
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextMobileNumberExample")
    public WebElement fld_mobileNumber;

    //Button continue
    @AndroidFindBy(id = "com.parsifal.starz:id/buttonContinue")
    public WebElement btn_Continue;


}