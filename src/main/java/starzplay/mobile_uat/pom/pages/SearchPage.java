package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class SearchPage extends BasePage {

    //Button search
    @FindBy(id = "com.parsifal.starz:id/search_mag_icon")
    private WebElement btn_Search;

    //Field search
    @FindBy(id = "com.parsifal.starz:id/search_src_text")
    private WebElement fld_Search;

    //List of movies displayed after making a search
    @FindBy(id = "com.parsifal.starz:id/textViewTitle")
    private List<WebElement> moviesSearched;

    public void waitForMoviesSearched(){
        new WebDriverWait(driver, 45).until(ExpectedConditions.presenceOfElementLocated(By.id("com.parsifal.starz:id/textViewTitle")));
    }

    public List<WebElement> checkMovies() {
        return moviesSearched;
    }
}