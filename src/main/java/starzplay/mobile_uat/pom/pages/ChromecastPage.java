package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class ChromecastPage extends BasePage {

    //Button back
    @FindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.ImageButton")
    public WebElement btn_Back;

    //Text chromecast device name
    @FindBy(id = "com.parsifal.starz:id/textview2")
    public WebElement txt_chromecastDeviceName;

    //Webelement chromecast devices controllers
    @FindBy(id = "com.parsifal.starz:id/controllers")
    public WebElement webEl_chromecastControllers;

    //Text content information
    @FindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.TextView")
    public WebElement txt_contentInfo;

    //Image content information
    @FindBy(id = "com.parsifal.starz:id/pageview")
    public WebElement img_contentInfo;


}