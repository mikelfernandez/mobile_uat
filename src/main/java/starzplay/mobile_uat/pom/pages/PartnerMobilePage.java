package starzplay.mobile_uat.pom.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;


public class PartnerMobilePage extends BasePage {

    public PartnerMobilePage() {
    }

    /**
     * Header section
     */

    //Button Contact us
    @FindBy(id = "msisdn")
    private WebElement fld_msisdn;

    //Text Plan
    @FindBy(xpath = "//*[@id='subConfirm']/h3/b")
    private WebElement txt_duration;

    //Button Subscribe now
    @FindBy(id = "confirm")
    private WebElement btn_subscribe;

    //Text Price
    @FindBy(xpath = "//*[@id='confirm']/span")
    private WebElement txt_price;

    public boolean isWeekly(){
        String toValidate = "STARZPlayVoucher";
        System.out.print("******Url: " + driver.getCurrentUrl().toLowerCase());
        return driver.getCurrentUrl().toLowerCase().contains(toValidate.toLowerCase());

    }

    public boolean isMonthly(){
        String toValidate = "STARZPlayMonthly";
        return driver.getCurrentUrl().toLowerCase().contains(toValidate.toLowerCase());

    }


}