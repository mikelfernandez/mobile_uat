package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.BasePage;


public class AccountValidationSecondPage extends BasePage {

    //Text page name
    @AndroidFindBy(id = "com.parsifal.starz:id/toolbar_left_arrow")
    public WebElement btn_Back;

    //Image icon ticked
    @AndroidFindBy(id = "com.parsifal.starz:id/imageViewTickIcon")
    public WebElement img_iconTicked;

    //Text validate account
    @AndroidFindBy(id = "com.parsifal.starz:id/textViewValidateAccount")
    public WebElement txt_valAccount;

    //Field verification code
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextVerificationCode")
    public WebElement fld_verificiationCode;

    //Button continue
    @AndroidFindBy(id = "com.parsifal.starz:id/buttonContinue")
    public WebElement btn_Continue;


}