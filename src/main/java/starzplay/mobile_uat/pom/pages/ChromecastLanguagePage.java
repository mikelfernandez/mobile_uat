package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class ChromecastLanguagePage extends BasePage {

    //Text chromecast language page title
    @FindBy(id = "com.parsifal.starz:id/dialog_cast_title_language")
    public WebElement txt_pageTitle;

    //Text chromecast audio availables
    @FindBy(id = "com.parsifal.starz:id/dialog_cast_caption_audio_item_text")
    public List<WebElement> txt_chromecastAudio;

    //Text chromecast subtitles availables
    @FindBy(id = "com.parsifal.starz:id/dialog_cast_caption_audio_item_text")
    public List<WebElement> txt_chromecastSubtitles;



}