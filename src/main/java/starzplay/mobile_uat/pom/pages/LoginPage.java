package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import starzplay.mobile_uat.pom.BasePage;
import starzplay.mobile_uat.utility.CommonActions;
import starzplay.mobile_uat.utility.Constant;


public class LoginPage extends BasePage {

    //Image starz logo
    @AndroidFindBy(id = "com.parsifal.starz:id/imageViewLogo")
    private WebElement img_Logo;

    //Field user email
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextUser")
    private WebElement fld_UserEmail;

    //Field user password
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextPass")
    private WebElement fld_UserPassw;

    //Button Login
    @AndroidFindBy(id = "com.parsifal.starz:id/buttonLogin")
    private WebElement btn_Login;

    //Button Facebook login
    @AndroidFindBy(id = "com.parsifal.starz:id/buttonFacebook")
    private WebElement btn_FBLogin;


    public void makeLogin(String arg1, String arg2){
        fld_UserEmail.sendKeys(arg1);
        CommonActions.clickBackButton();
        fld_UserPassw.sendKeys(arg2);
        btn_Login.click();
    }

    public void makeLoginFacebook(){
        new WebDriverWait(driver, Constant.WAIT_MEDIUM).until(ExpectedConditions.visibilityOf(btn_FBLogin));
        btn_FBLogin.click();

    }

    public boolean loginButtonIsDisplayed() {
        new WebDriverWait(driver, Constant.WAIT_MEDIUM).until(ExpectedConditions.visibilityOf(btn_Login));
        return btn_Login.isDisplayed();
    }


}