package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.TouchAction;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class WatchlistPage extends BasePage {

    List<String> title;


    //Text title of the page
    @FindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.TextView")
    public WebElement txt_pageTitle;

    //WebElement watchlist options for tablet (WATCHING, WATCH LIST, HISTORY)
    @FindBy(id = "com.parsifal.starz:id/watchlist_view")
    public WebElement webEl_watchlistOptions;

    //WebElement item list
    @FindBy(id = "com.parsifal.starz:id/lv_items_list")
    public WebElement webEl_itemList;

    //Tab Watching
    @FindBy(id = "com.parsifal.starz:id/rb_watchlist")
    public WebElement tab_watching;

    //Tab Watchlist
    @FindBy(id = "com.parsifal.starz:id/rb_wishlist")
    public WebElement tab_watchlist;

    //Tab History
    @FindBy(id = "com.parsifal.starz:id/rb_watchedlist")
    public WebElement tab_history;

    //List of content displayed
    @FindBy(xpath = "com.parsifal.starz:id/tv_title")
    public List<WebElement> contentDisplayed;

    public List<String> moviesTitleInWatchlist() {
        for (WebElement elem: contentDisplayed){
            title.add(elem.getText());
        }

        return title;
    }


    public boolean checkMoviesInWatchlist(String title) {
        for (WebElement elem : contentDisplayed){
            if (elem.getText().equals(title))
                return true;
        }
        return false;
    }

    public void longPress(WebElement elem){
        TouchAction action = new TouchAction((AppiumDriver)driver);
        action.longPress(elem,4000).perform();
    }
}