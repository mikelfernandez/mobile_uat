package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import starzplay.mobile_uat.pom.BasePage;
import starzplay.mobile_uat.utility.CommonActions;
import starzplay.mobile_uat.utility.Constant;

import java.util.List;

public class HomePage extends BasePage {

    //Button burger Menu
    @AndroidFindBy(xpath = "//android.view.View[@resource-id='com.parsifal.starz:id/toolbar']//android.widget.ImageButton")
    private WebElement btn_burgerMenu;

    //Image hero content
    @AndroidFindBy(id = "com.parsifal.starz:id/pageritem_hero_image")
    private WebElement img_HeroContent;

    //Button  Watch now/Resume
    @AndroidFindBy(id = "com.parsifal.starz:id/module_hero_play_now_btn")
    private WebElement btn_watchNow;

    //Button Watch Trailer
    @AndroidFindBy(id = "com.parsifal.starz:id/module_hero_trailer_btn")
    private WebElement btn_WatchTrailer;

    //Text module header title
    @AndroidFindBy(id = "com.parsifal.starz:id/module_header_title")
    private WebElement txt_modHeadTitle;

    //Button add Watchlist
    @AndroidFindBy(id = "com.parsifal.starz:id/module_hero_watchlist_btn")
    private List<WebElement> btn_addWatchlist;

    //Button search
    @AndroidFindBy(id = "com.parsifal.starz:id/action_search")
    private WebElement btn_search;
    //Button chromecast
    @AndroidFindBy(xpath= "//android.support.v7.widget.LinearLayoutCompat/android.view.View")
    private WebElement btn_Chromecast;

    //Button chromecast
    @AndroidFindBy(xpath= "//android.support.v7.widget.LinearLayoutCompat/android.view.View")
    private List<WebElement> list_btn_Chromecast;


    public boolean chromecastButtonIsDisplayed(){
        return list_btn_Chromecast.size() >0;

    }

    public void openBurgerMenu(){
        new WebDriverWait(driver, Constant.WAIT_SHORT).until(ExpectedConditions.elementToBeClickable(btn_burgerMenu));
        btn_burgerMenu.click();
    }

    public boolean heroIsDisplayed(){
        return img_HeroContent.isDisplayed() || btn_watchNow.isDisplayed();

    }

    public void navigateToSection(String text){
        while (!txt_modHeadTitle.getText().equals(text.toUpperCase())) {
            CommonActions.swipeUpShort();
        }
    }

    public void goToSection(String arg1){
        if (txt_modHeadTitle.getText().equals(arg1)) {
            txt_modHeadTitle.click();
        }
    }

    public boolean addContentButtonIsDisplayed(){
        return btn_addWatchlist.size() >0;

    }


    public boolean isNotLogged() {
        return !chromecastButtonIsDisplayed();

    }

    public boolean isLogged() {
        return !chromecastButtonIsDisplayed();

    }



}
