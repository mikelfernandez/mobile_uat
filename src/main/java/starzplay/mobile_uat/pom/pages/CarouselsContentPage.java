package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.BasePage;


public class CarouselsContentPage extends BasePage {

    //Text page name
    @AndroidFindBy(id = "com.parsifal.starz:id/module_catalogue_title")
    public WebElement txt_PageName;

}