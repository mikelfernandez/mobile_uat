package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class ChromecastConnectionPage extends BasePage {

    //Button search
    @FindBy(id = "android:id/title")
    public WebElement txt_pageTitle;

    //Webelement chromecast devices list
    @FindBy(id = "com.parsifal.starz:id/media_route_list")
    public WebElement webEl_chromecastDevices;


}