package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class WatchlistActionsPage extends BasePage {

    //Text content title
    @FindBy(id = "com.parsifal.starz:id/tv_title")
    public WebElement txt_contentTitle;

    //Button remove
    @FindBy(id = "com.parsifal.starz:id/ib_remove_from_watchlist")
    public WebElement btn_Remove;

    //Button share
    @FindBy(id = "com.parsifal.starz:id/ib_share")
    public WebElement btn_Share;


}