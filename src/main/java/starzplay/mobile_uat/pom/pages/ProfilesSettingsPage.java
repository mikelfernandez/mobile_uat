package starzplay.mobile_uat.pom.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class ProfilesSettingsPage extends BasePage {

    //Text page name
    @FindBy(id = "com.parsifal.starz:id/textViewPersonalInformation")
    public WebElement txt_personalInf;

    //Image profile picture
    @FindBy(id = "com.parsifal.starz:id/imageViewProfilePicture")
    public WebElement img_profilePicture;

    //First name field
    @FindBy(id = "com.parsifal.starz:id/editTextFirstName")
    public WebElement fld_userFirstName;

    //Last name field
    @FindBy(id = "com.parsifal.starz:id/editTextLastName")
    public WebElement fld_userLastName;

    //Button Edit Email
    @FindBy(id = "com.parsifal.starz:id/imageButtonEditEmail")
    public WebElement btn_editEmail;

    //Button Edit Email
    @FindBy(id = "com.parsifal.starz:id/editTextEmail")
    public WebElement fld_Email;

    //Button Edit Email
    @FindBy(id = "com.parsifal.starz:id/editTextPassword")
    public WebElement fld_Pass;

    //Button Edit Password
    @FindBy(id = "com.parsifal.starz:id/imageButtonEditPassword")
    public WebElement btn_editPass;

    //Day of Birth Spinner
    @FindBy(id = "com.parsifal.starz:id/spinnerDay")
    public WebElement spn_day;

    //Month of Birth Spinner
    @FindBy(id = "com.parsifal.starz:id/spinnerMonth")
    public WebElement spn_Month;

    //Year of Birth Spinner
    @FindBy(id = "com.parsifal.starz:id/spinnerYear")
    public WebElement spn_Year;

    //Button Male
    @FindBy(id = "com.parsifal.starz:id/radioButtonMale")
    public WebElement btn_male;

    //Button Female
    @FindBy(id = "com.parsifal.starz:id/radioButtonFemale")
    public WebElement btn_female;

    //Country Spinner
    @FindBy(id = "com.parsifal.starz:id/spinnerCountry")
    public WebElement spn_country;

    //Mobile country Spinner
    @FindBy(id = "com.parsifal.starz:id/spinnerCountryFlags")
    public WebElement spn_mobileCountry;

    //Mobile phone field
    @FindBy(id = "com.parsifal.starz:id/editTextMobileNumber")
    public WebElement fld_mobilePhone;

    //Button Save Changes
    @FindBy(id = "com.parsifal.starz:id/buttonSaveChanges")
    public WebElement btn_saveChanges;

    //Button logout
    @FindBy(id = "com.parsifal.starz:id/buttonLogout")
    public WebElement btn_Logout;

    public boolean logoutButtonIsDisplayed(){
        return driver.findElements(By.id("com.parsifal.starz:id/buttonLogout")).size() >0;
    }
}
