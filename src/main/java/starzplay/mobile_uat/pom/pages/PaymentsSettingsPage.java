package starzplay.mobile_uat.pom.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaymentsSettingsPage extends SettingsPage{


    //Button Change Payment
    @FindBy(id = "com.parsifal.starz:id/buttonChangePayment")
    public WebElement btn_changePayment;

    //Button Change Payment
    @FindBy(id = "com.parsifal.starz:id/textViewAccountStatus")
    public WebElement txt_AccountStatus;

    //Text Discount and Vouchers Type
    @FindBy(id = "com.parsifal.starz:id/textViewTypeFill")
    public WebElement txt_discountType;

    //Text expiration date
    @FindBy(id = "com.parsifal.starz:id/textViewExpiryDateFill")
    public WebElement txt_expiration;
}
