package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import starzplay.mobile_uat.pom.BasePage;

public class FacebookBrowserPage extends BasePage {


    //field email
    @FindBy(id = "email")
    private WebElement fld_email;

    //field password
    @FindBy(id = "pass")
    private WebElement fld_pass;

    //Button log in
    @FindBy(id = "loginbutton")
    private WebElement btn_login;

    //Button log in
    @FindBy(id = "userNavigationLabel")
    private WebElement btn_options;

    //Button log out
    @FindBy(xpath = "//div/div/div[1]/div/div/ul/li[12]")
    private WebElement btn_logout;

    public void fillEmail(String email){
        new WebDriverWait(driver,20).until(ExpectedConditions.elementToBeClickable(fld_email));
        fld_email.sendKeys(email);
    }

    public void fillPass(String pass){
        new WebDriverWait(driver,20).until(ExpectedConditions.elementToBeClickable(fld_pass));
        fld_pass.sendKeys(pass);
    }

    public void goLogin(){
        new WebDriverWait(driver,20).until(ExpectedConditions.elementToBeClickable(btn_login));
        btn_login.click();
    }

    public void openOptions(){
        new WebDriverWait(driver,20).until(ExpectedConditions.elementToBeClickable(btn_options));
        btn_options.click();
    }

    public void makeLogout(){
        new WebDriverWait(driver,20).until(ExpectedConditions.elementToBeClickable(btn_logout));
        btn_logout.click();
    }
}
