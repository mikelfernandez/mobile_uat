package starzplay.mobile_uat.pom.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import starzplay.mobile_uat.pom.BasePage;

public class ChromecastDisconnectionPage extends BasePage {

    //Button search
    @FindBy(id = "com.parsifal.starz:id/route_name")
    public WebElement txt_chromecastDeviceName;

    //Text stop chromecast connection
    @FindBy(id = "com.parsifal.starz:id/stop")
    public WebElement btn_stopChromecast;


}