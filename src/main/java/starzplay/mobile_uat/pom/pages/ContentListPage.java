package starzplay.mobile_uat.pom.pages;


import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.BasePage;

import java.util.List;

public class ContentListPage extends BasePage {

    //Content list page title
    @AndroidFindBy(id = "com.parsifal.starz:id/module_catalogue_title")
    public WebElement txt_PageName;

    //WebElement character list
    @AndroidFindBy(id = "com.parsifal.starz:id/iv_title_thumb")
    public List<WebElement> img_contentThumb;

    //Image character list
    @AndroidFindBy(id = "com.parsifal.starz:id/tv_title")
    public WebElement txt_contentTitle;

    //WebElement hero
    @AndroidFindBy(id = "com.parsifal.starz:id/tv_year")
    public WebElement txt_contentYear;




}