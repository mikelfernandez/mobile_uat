package starzplay.mobile_uat.pom.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.BasePage;


public class AccountCreationPage extends BasePage {


    //Text page name
    @AndroidFindBy(id = "com.parsifal.starz:id/toolbar_left_arrow")
    public WebElement btn_Back;

    //Text page name
    @AndroidFindBy(id = "com.parsifal.starz:id/textViewCreateAccount")
    public WebElement txt_pageName;

    //Text page name
    @AndroidFindBy(id = "com.parsifal.starz:id/textViewPromotionalDescription")
    public WebElement txt_PromoDesc;

    //Field first name
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextFirstName")
    public WebElement fld_firstName;

    //Field last name
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextLastName")
    public WebElement fld_lastName;

    //Field email
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextEmail")
    public WebElement fld_email;

    //Field confirm email
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextConfirmEmail")
    public WebElement fld_confirmEmail;

    //Field password
    @AndroidFindBy(id = "com.parsifal.starz:id/editTextPassword")
    public WebElement fld_password;

    //Checkbox terms and conditions
    @AndroidFindBy(id = "com.parsifal.starz:id/checkBoxTermsConditions")
    public WebElement check_termsCond;

    //Button continue
    @AndroidFindBy(id = "com.parsifal.starz:id/buttonContinue")
    public WebElement btn_Continue;


}