package starzplay.mobile_uat.utility;


import cucumber.api.Scenario;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import starzplay.mobile_uat.managers.testrail.APITestRailManager;
import starzplay.mobile_uat.pom.BasePage;

import java.util.HashMap;
import java.util.Map;

import static starzplay.mobile_uat.pom.BasePage.driver;

public class GeneralUtils {


    public static void setStepName(String object, int status){
        Map step1 = new HashMap();
        step1.put("status_id",status);
        step1.put("content", object.replaceAll("_", " "));
        APITestRailManager.steps.add(step1);
    }

    public static void embedScreenshot(Scenario scenario) {
        try {
            byte[] screenshot = (driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        } catch (WebDriverException wde) {
            System.err.println(wde.getMessage());
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }
    }

        public static void addCookie(String name,String content){
        Cookie cookie = new Cookie(name, content);
        driver.manage().addCookie(cookie);
    }


}
