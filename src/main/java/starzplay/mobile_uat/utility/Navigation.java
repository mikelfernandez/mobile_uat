package starzplay.mobile_uat.utility;


import starzplay.mobile_uat.pom.BasePage;

public class Navigation extends BasePage {

    public static String getTrunkPortalURL() {
        String trunk = null;
        String env = System.getProperty("environment");
        switch (env){
            case "prod":
                trunk = "https://arabia.starzplay.com";
                break;
            case "stage":
                trunk = "https://" + Constant.portal_user_name + ":" + Constant.portal_password + "@" + "staging.playco.com";
                break;
            case "test":
                trunk = "https://" + Constant.portal_user_name + ":" + Constant.portal_password + "@" + "portal.playco.com";
                break;
            case "hotfix":
                trunk = "https://" + Constant.portal_user_name + ":" + Constant.portal_password + "@" + "portal-hotfix.playco.com";
                break;
            case "local":
                String address = System.getProperty("serverAddress");
                String port = System.getProperty("serverPort");
                if (address != null && !address.isEmpty()) {
                    trunk = "http://"+address;
                    if (port != null && !port.isEmpty()) {
                        trunk += ":"+port;
                    }
                }
                else
                    trunk = "http://localhost:8888";
                break;
        }
        return trunk;
    }

    public static void goToPage(String page, String language) {
        String url = Navigation.getTrunkPortalURL();
        switch (page) {
            case "HomePage":
                url +="/" +language + "/start";
                //Cookie cookie = new Cookie("dontshowlandingpage", "true");
                //BasePage.driver.manage().addCookie(cookie);
                break;
            case "ArabicPage":
                url +="/" +language + "/arabic";
                //Cookie cookie = new Cookie("dontshowlandingpage", "true");
                //BasePage.driver.manage().addCookie(cookie);
                break;
            case "whyStarzPlay":
                url +="/" +language + "/p/whyStarzPlay";
                break;
            case "Watchlist":
                url +="/watchlist";
                break;
            case "FAQ":
                url += "/" +language + "/faq";
                break;
            case "Contact":
                url += "/" +language + "/contact";
                break;
            case "AboutUs":
                url += "/" +language + "/aboutus";
                break;
            case "TermsAndConditions":
                url += "/" +language + "/termsandconditions";
                break;
            case "PrivacyPolicy":
                url += "/" +language + "/privacypolicy";
                break;
            case "movies":
                url += "/" +language + "/movies";
                break;
            case "series":
                url += "/" + language + "/series";
                break;
            case "navigation":
                url += "/" + language + "/navigation";
                break;
            case "kids":
                url += "/" + language + "/kids";
                break;
            case "AEPage":
                url += "/" + language + "/AE";
                break;
            case "PaymentsPage":
                url += "/settings/payment";
                break;
            case "LandingPageOoredoo":
                url += "/" + language + "/partners/ooredoo-tunisia";
                break;
            case "LandingPageOoredooKuwait":
                url += "/" + language + "/partners/ooredoo-kuwait";
                break;
            case "LandingPageVodafone":
                url += "/" + language + "/partners/vodafone-egypt";
                break;
            case "LandingPageOrange":
                url +="/" + language + "/partners/orange-jordan";
                break;
            case "LandingPageEtisalat":
                url +="/" + language + "/partners/etisalat-uae";
                break;
            case "LandingPageViva":
                url += "/" + language + "/partners/viva-bahrain";
                break;
            case "LandingPageDu":
                url += "/" + language + "/partners/du-uae";
                break;
            case "LandingPageSTC":
                url += "/" + language + "/partners/stc-saudi";
                break;
            case "LandingPage":
                url += "/" + language + "/landing";
                break;
            case "LandingPagePlaystation":
                url += "/" + language + "/partners/sony-playstation";
                break;
            case "AffiliatePage":
                url += "/" + language +  "/affiliate";
                break;
            case "Sitemap":
                url += "/sitemap.xml";
                break;
            case "Login":
                url+="/login?lang=" + language;
                break;
            case "Signup":
                url+="/registration/userinfo";
                break;

        }
        System.out.println(url);
        driver.get(url);
    }

    public static void goFacebookPage() {
        String url = "http://www.facebook.com";
        driver.get(url);
    }

}
