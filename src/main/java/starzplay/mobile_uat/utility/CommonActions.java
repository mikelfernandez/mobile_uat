package starzplay.mobile_uat.utility;

import cucumber.api.Scenario;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.*;
import starzplay.mobile_uat.pom.BasePage;
import java.util.List;

/**
 * Created by mikelfernandez on 20/11/15.
 */
public class CommonActions extends BasePage {


    //Button chromecast
    @AndroidFindBy(xpath= "//android.support.v7.widget.LinearLayoutCompat/android.view.View")
    public static WebElement btn_Chromecast;

    //Button chromecast
    @AndroidFindBy(xpath= "//android.support.v7.widget.LinearLayoutCompat/android.view.View")
    public static List<WebElement> list_btn_Chromecast;

    public static void swipeRightToOpenMenu(){
        Dimension size = driver.manage().window().getSize();
        int endx= (int) (size.width * 0.8);
        int startx = (int) (size.width * 0.01);
        int starty = size.height / 2;
        ((AppiumDriver)driver).swipe(startx, starty, endx, starty, 1000);

    }

    public static void swipeRight(){
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.8);
        int endx= (int) (size.width * 0.2);
        int starty = size.height / 2;
        ((AppiumDriver)driver).swipe(startx, starty, endx, starty, 500);

    }

    public static void swipeLeft(){
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.2);
        int endx= (int) (size.width * 0.8);
        int starty = size.height / 2;
        ((AppiumDriver)driver).swipe(startx, starty, endx, starty, 1000);

    }

    public static void swipeUp()  {
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.6);
        int starty = (int) (size.height * 0.7);
        int endy = (int) (size.height * 0.3);
        ((AppiumDriver)driver).swipe(startx, starty, startx, endy, 500);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void swipeUpShort()  {
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.5);
        int starty = (int) (size.height * 0.2);
        int endy = (int) (size.height * 0.1);
        ((AppiumDriver)driver).swipe(startx, starty, startx, endy, 200);

    }

    public static void swipeDown()  {
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.6);
        int starty = (int) (size.height * 0.3);
        int endy = (int) (size.height * 0.7);
        ((AppiumDriver)driver).swipe(startx, starty, startx, endy, 500);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void swipeDownShort()  {
        Dimension size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.5);
        int starty = (int) (size.height * 0.1);
        int endy = (int) (size.height * 0.2);
        ((AppiumDriver)driver).swipe(startx, starty, startx, endy, 200);

    }

    public static boolean isTablet(){
        boolean isTablet;
        int height = driver.manage().window().getSize().getHeight();
        int width = driver.manage().window().getSize().getWidth();
        isTablet = height < width;

        return isTablet;
    }

    public static void clickBackButton(){
        driver.navigate().back();

    }

    public static void resetApp(){
        ((AppiumDriver)driver).resetApp();

    }

    public static void scrollForTabs(String tab){
        ((AppiumDriver)driver).scrollTo(tab);

    }

    public static boolean chromecastButtonIsDisplayed(){
        return list_btn_Chromecast.size() >0;

    }

    public static void embedScreenshot(Scenario scenario) {
        try {
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenshot, "image/png");
        } catch (WebDriverException wde) {
            System.err.println(wde.getMessage());
        } catch (ClassCastException cce) {
            cce.printStackTrace();
        }
    }


    public static String getLanguage(){
        WebElement element = driver.findElement(By.xpath("html"));
        return element.getAttribute("lang");
    }
}
