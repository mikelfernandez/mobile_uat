package starzplay.mobile_uat.resources.Users;

public class User {

    private static String user_name;
    private static String user_password;
    private static String user_phone;
    private String user_status;

    public User(String name, String password,String phone) {
        user_name = name;
        user_password = password;
        user_phone = phone;
        System.out.println("username: " + name +  ", userbillingPhone: " + user_phone + ", userPass: " + user_password);
    }

    public static void setUserName(String name) {
        user_name = name;
    }

    public static String getUserName(){
        return user_name;
    }

    public static void setUserPhone(String phone){
        user_phone = phone;
    }

    public static String getUserPhone(){
        return user_phone;
    }

    public static void setUserPassword(String pass) {
        user_password = pass;
    }

    public static String getUserPassword(){
        return user_password;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    public String getUser_status() {
        return user_status;
    }

}
