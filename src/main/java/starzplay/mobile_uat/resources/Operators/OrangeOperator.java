package starzplay.mobile_uat.resources.Operators;

/**
 * Created by markelarizaga on 25/04/16.
 */
public class OrangeOperator extends Operator {

    public OrangeOperator(){
        this.name = "Orange";
        this.validTelephoneNumber = "779890199";
        this.countryPrefix = "962";

        this.paymentName = "orange_jo";
        this.langFileName = "orange-jordan";

    }
}
