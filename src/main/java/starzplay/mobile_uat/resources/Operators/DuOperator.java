package starzplay.mobile_uat.resources.Operators;

/**
 * Created by markelarizaga on 25/04/16.
 */
public class DuOperator extends Operator {

    public DuOperator(){
        this.name = "Du";

        this.validTelephoneNumber = "529752712";
        this.countryPrefix = "971";

        this.paymentName = "du";
        this.langFileName = "du-uae";

        this.ddbbWeeklyCode = "du";
        this.ddbbMonthlyCode = "du";

    }
}
