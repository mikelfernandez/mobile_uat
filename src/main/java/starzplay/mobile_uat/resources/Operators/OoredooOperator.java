package starzplay.mobile_uat.resources.Operators;

/**
 * Created by markelarizaga on 25/04/16.
 */
public class OoredooOperator extends Operator {

    public OoredooOperator(){
        this.name = "Ooredoo";
        this.validTelephoneNumber = "22170561";
        this.notvalidTelephoneNumber = "22170561";
        this.countryPrefix = "216";

        this.paymentName = "ooredoo_tn";
        this.langFileName = "ooredoo-tunisia";

    }

}
