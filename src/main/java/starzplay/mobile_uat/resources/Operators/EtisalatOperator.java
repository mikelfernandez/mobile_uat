package starzplay.mobile_uat.resources.Operators;

/**
 * Created by markelarizaga on 25/04/16.
 */
public class EtisalatOperator extends Operator {

    public EtisalatOperator(){
        this.name = "Etisalat";
        this.telephoneNumberLength = 6;
        this.telephoneFirstNumbers = "52";
        this.countryPrefix = "971";

        this.paymentName = "etisalat_tn";
        this.langFileName = "etisalat-tunisia";

    }
}
