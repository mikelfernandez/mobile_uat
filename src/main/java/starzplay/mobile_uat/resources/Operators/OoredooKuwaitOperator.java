package starzplay.mobile_uat.resources.Operators;


public class OoredooKuwaitOperator extends Operator {

    public OoredooKuwaitOperator(){
        this.name = "Ooredoo";
        this.countryPrefix = "965";
        this.telephoneFirstNumbers = "6770";
        this.telephoneNumberLength = 9;
        this.notvalidTelephoneNumber = "22170561";

        this.paymentName = "ooredoo_kw";
        this.langFileName = "ooredoo-kuwait";

    }

}
