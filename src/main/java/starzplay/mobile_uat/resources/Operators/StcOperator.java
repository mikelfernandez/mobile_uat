package starzplay.mobile_uat.resources.Operators;

/**
 * Created by markelarizaga on 25/04/16.
 */
public class StcOperator extends Operator {

    public StcOperator(){
        this.name = "STC";
        this.country = "saudi";
        this.validTelephoneNumber = "534005805";
        this.notvalidTelephoneNumber = "111111";
        this.countryPrefix = "966";

        this.paymentName = "stc_sa";
        this.langFileName = "stc-saudi";

    }
}
