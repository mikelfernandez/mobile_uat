package starzplay.mobile_uat.resources.Operators;


import starzplay.mobile_uat.managers.APIRequestManager;

import java.util.HashMap;

/**
 * Created by markelarizaga on 25/04/16.
 */
public class Operator {

    protected String name;
    protected String country;
    protected String paymentName;
    protected String langFileName;
    protected String countryPrefix;
    protected String telephoneFirstNumbers;
    protected String validTelephoneNumber;
    protected String notvalidTelephoneNumber;
    protected String registeredTelephoneNumber;
    protected int telephoneNumberLength;
    protected String period;
    protected HashMap<String, HashMap<String, String>> copies;
    protected String ddbbWeeklyCode;
    protected String ddbbMonthlyCode;

    public String getValidTelephoneNumber(){
        String mobile = null;
        if(this.validTelephoneNumber != null){
            mobile = this.validTelephoneNumber;
        } else {
            long currentTime = System.currentTimeMillis();
            mobile = ("" + currentTime).substring(this.telephoneNumberLength);
            mobile = this.telephoneFirstNumbers + mobile;
        }

        return mobile;
    }

    public String getRegisteredTelephoneNumber(){
        String mobile = null;
        if(this.registeredTelephoneNumber != null){
            mobile = this.registeredTelephoneNumber;
        } else {
            mobile = this.telephoneFirstNumbers + mobile;
        }

        return mobile;
    }

    public String getNotValidTelephoneNumber(){
        String mobile = null;
        if(this.notvalidTelephoneNumber != null){
            mobile = this.notvalidTelephoneNumber;
        } else {
            long currentTime = System.currentTimeMillis();
            mobile = ("" + currentTime).substring(this.telephoneNumberLength);
            mobile = this.telephoneFirstNumbers + mobile;
        }

        return mobile;
    }

    public String getLangFileName() {
        return langFileName;
    }

    public String getName() {return name;}

    public String getPaymentName() {return paymentName;}

    public String getCountry() {return country;}

    public String getCountryPrefix() {return countryPrefix;}


    public String getWeeklyPrice() {

        return APIRequestManager.getLocalPrice(paymentName, true);
    }

    public String getMonthlyPrice() {

        return APIRequestManager.getLocalPrice(paymentName, false);
    }

    public String getWeeklyPeriod() {

        return "per week";
    }

    public String getMonthlyPeriod() {

        return "per month";
    }

    public String getCurrency() {

        return APIRequestManager.getLocalCurrency(paymentName);
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getPeriod() {
        return period;
    }

    public String getDdbbWeeklyCode() {

        return paymentName + "_week";
    }

    public String getDdbbMonthlyCode() {

        return paymentName;
    }

    public HashMap<String, HashMap<String, String>> getCopies() {
        return copies;
    }



}
