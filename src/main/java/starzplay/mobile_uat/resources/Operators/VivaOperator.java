package starzplay.mobile_uat.resources.Operators;


/**
 * Created by markelarizaga on 25/04/16.
 */
public class VivaOperator extends Operator {

    public VivaOperator() {
        this.name = "viva";
        this.country = "bahrain";
        this.telephoneNumberLength = 7;
        this.countryPrefix = "973";
        this.telephoneFirstNumbers = "37";

        this.paymentName = "viva_bh";
        this.langFileName = "viva-bahrain";

    }

}
