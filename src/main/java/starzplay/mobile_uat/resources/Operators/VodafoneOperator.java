package starzplay.mobile_uat.resources.Operators;

/**
 * Created by markelarizaga on 25/04/16.
 */
public class VodafoneOperator extends Operator {

    public VodafoneOperator(){
        this.name = "Vodafone";

        this.validTelephoneNumber = "1095603461";
        this.countryPrefix = "20";

        this.paymentName = "vodafone_eg";
        this.langFileName = "vodafone-egypt";


    }
}
