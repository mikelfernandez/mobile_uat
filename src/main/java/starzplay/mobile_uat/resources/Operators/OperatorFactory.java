package starzplay.mobile_uat.resources.Operators;

/**
 * Created by markelarizaga on 25/04/16.
 */
public class OperatorFactory {

    public static Operator getOperator(String operatorName){
        Operator operator = null;
        switch (operatorName.toLowerCase()){
            case "du":
                operator = new DuOperator();
                break;
            case "etisalat":
                operator = new EtisalatOperator();
                break;
            case "viva":
                operator = new VivaOperator();
                break;
            case "ooredoo":
                operator = new OoredooOperator();
                break;
            case "ooredookuwait":
                operator = new OoredooKuwaitOperator();
                break;
            case "stc":
                operator = new StcOperator();
                break;
            case "orange":
                operator = new OrangeOperator();
                break;
            case "vodafone":
                operator = new VodafoneOperator();
                break;
        }
        return operator;
    }
}
