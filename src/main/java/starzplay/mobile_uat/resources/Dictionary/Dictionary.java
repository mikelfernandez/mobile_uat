package starzplay.mobile_uat.resources.Dictionary;


import starzplay.mobile_uat.managers.TranslationLanguagesManager;
import starzplay.mobile_uat.resources.Operators.Operator;
import starzplay.mobile_uat.resources.Operators.OperatorFactory;
import starzplay.mobile_uat.utility.CommonActions;

/**
 * Created by mikelfernandez on 27/5/16.
 */
public class Dictionary {

    static Operator operator;
    static String opFilename;
    static String opName;
    static String opPeriod;
    static String opCurrency;

    public static void setDictionay(String partner) {
        operator = OperatorFactory.getOperator(partner);
        opFilename = operator.getLangFileName();
        opName = operator.getName();
        opPeriod = operator.getPeriod();
        opCurrency = operator.getCurrency();

    }

    // Global functions
    public static String getText(String folder, String file, String key){
        return TranslationLanguagesManager.getTranslation(CommonActions.getLanguage(), folder, file, key).trim();
    }

    public static String getTextPayment(String key) {

        return getText("paymentMethods", opFilename, "dict.settings.payments." + opName + "." + key);
    }

    public static String getTextRegistration(String key) {
        if (key.indexOf("dict") < 0) {
            key = "dict.registration." + key;
        }
        return getText("registration", "registration", key);
    }

    public static String getTextLanding(String key) {
        return getText("landingpage", "landingpage", "dict.landing." + key);
    }

    public static String getTextLandingPartner(String key) {
        return getText("landingpage", opFilename, "dict.landing." + opFilename + "." + key);
    }


    //Especial cases payment
    public static String get_weeklyPlan(){
        return HtmlSanitizer.replacePlaceholders(getTextPayment("weeklyPlan"), operator.getWeeklyPrice()) + " " + operator.getCurrency();
    }

    public static String get_monthlyPlan(){
        return HtmlSanitizer.replacePlaceholders(getTextPayment("monthlyPlan"), operator.getWeeklyPrice()) + " " + operator.getCurrency();
    }

    public static String get_disclaimer_no_prospectWeekly(){
        return HtmlSanitizer.replacePlaceholders(getTextPayment("disclaimer_no_prospect"), operator.getWeeklyPrice() + " " + opCurrency, operator.getWeeklyPeriod());
    }

    public static String get_disclaimer_no_prospectMonthly(){
        return HtmlSanitizer.replacePlaceholders(getTextPayment("disclaimer_no_prospect"), operator.getMonthlyPrice() + " " + opCurrency, operator.getMonthlyPeriod());
    }

    public static String get_verificationcode(){
        return getText("paymentMethods", "opensignup", "dict.opensignup.verificationcode").trim();
    }

    public static String get_codesentto() {
        return getText("paymentMethods", "opensignup", "dict.opensignup.codesentto").trim();
    }
    ////////////////////////////


    //Especial cases landing partners
    public static String get_get() {
        return getTextLandingPartner("get").replace("<span id='operator-name'>Viva<\\/span>",operator.getName()).replace("<span id='trial-period'>30 day<\\/span>", "30").trim();
    }
    ////////////////////////////


    //Especial cases globals
    public static String get_missingTelephonePartnerLanding(){
        return getText("globals", "globals", "dict.index.error.requiredfield").trim();
    }
    ////////////////////////////


}
