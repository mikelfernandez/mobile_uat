package starzplay.mobile_uat.resources.Browsers;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import static starzplay.mobile_uat.pom.BasePage.driver;
import static starzplay.mobile_uat.utility.Constant.appiumMachine;

/**
 * Created by mikelfernandez on 27/6/16.
 */
public class AndroidBrowser extends Browser{

    public AndroidBrowser() throws MalformedURLException {


        this.name = "android";
        this.capabilities = new DesiredCapabilities();

        this.capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");
        this.capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "s3");
        this.capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "5");
        this.capabilities.setCapability(AndroidMobileCapabilityType.APP_WAIT_ACTIVITY, "com.parsifal.starz.activities.MainActivity");
        this.capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.parsifal.starz");
        this.capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.parsifal.starz.activities.SplashActivity");

        driver = new AndroidDriver(new URL(appiumMachine), this.capabilities);

    }
}
