package starzplay.mobile_uat.resources.Browsers;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import static starzplay.mobile_uat.pom.BasePage.driver;
import static starzplay.mobile_uat.utility.Constant.appiumMachine;

/**
 * Created by mikelfernandez on 27/6/16.
 */
public class IOSBrowser extends Browser{

    public IOSBrowser() throws MalformedURLException {


        this.name = "android";
        this.capabilities = new DesiredCapabilities();

        this.capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "iOS");
        this.capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Jaime Pont’s iPhone");
        this.capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9.0");
        this.capabilities.setCapability(MobileCapabilityType.UDID, "19680d1eaf1017cb11144fff5e0a0e2dd41d3c1d");
        this.capabilities.setCapability(MobileCapabilityType.APP, "com.parsifal.Starz");

        driver = new AndroidDriver(new URL(appiumMachine), this.capabilities);

    }
}
