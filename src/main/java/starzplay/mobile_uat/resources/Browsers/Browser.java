package starzplay.mobile_uat.resources.Browsers;

import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created by mikelfernandez on 7/5/16.
 */
public abstract class Browser {

    protected String name;
    protected DesiredCapabilities capabilities;
}
