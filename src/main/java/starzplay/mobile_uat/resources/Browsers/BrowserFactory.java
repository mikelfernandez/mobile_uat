package starzplay.mobile_uat.resources.Browsers;


import java.net.MalformedURLException;
import static starzplay.mobile_uat.pom.BasePage.driver;


public class BrowserFactory {

    public static Browser getBrowser(String browserName) throws MalformedURLException {
        Browser browser = null;
        switch (browserName.toLowerCase()) {
            case "android":
                browser = new AndroidBrowser();
                break;
            case "ios":
                browser = new IOSBrowser();
                break;
        }

        return browser;
    }

    public static void closeBrowser(){
        driver.quit();
        driver = null;
    }
}
