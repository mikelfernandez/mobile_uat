package starzplay.mobile_uat.stepDef;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import starzplay.mobile_uat.pom.components.TitleBar;
import starzplay.mobile_uat.pom.components.menus.BurgerMenu;
import starzplay.mobile_uat.pom.components.popup.DeactivateUserPopup;
import starzplay.mobile_uat.pom.components.popup.InvalidLoginPopup;
import starzplay.mobile_uat.pom.components.popup.LoginPopup;
import starzplay.mobile_uat.pom.pages.HomePage;
import starzplay.mobile_uat.pom.pages.InputPage;
import starzplay.mobile_uat.pom.pages.LoginPage;
import starzplay.mobile_uat.pom.pages.ProfilesSettingsPage;
import starzplay.mobile_uat.utility.CommonActions;


public class LoginStepDef {

    LoginPage login;
    HomePage home;
    BurgerMenu burgerMenu;
    LoginPopup loginPopup;
    TitleBar title;
    ProfilesSettingsPage profile;
    DeactivateUserPopup deactivateUser;
    InputPage input;
    InvalidLoginPopup invalidLoginPopup;

    @Given("^I am at the home page as a not logged user$")
    public void I_am_at_the_home_page_as_a_not_logged_user() throws Throwable {
        home = new HomePage();
        title = new TitleBar();
        title.btn_burgerMenu.click();
        burgerMenu = new BurgerMenu();
        Assert.assertTrue("I am at the home page as a not logged user", burgerMenu.btn_Settings.getText().contains("LOG IN"));
        CommonActions.clickBackButton();

    }

    @Then("^I am at the home page as a logged user$")
    public void I_am_at_the_home_page_as_a_logged_user() throws Throwable {
        home = new HomePage();
        title = new TitleBar();
        title.btn_burgerMenu.click();
        burgerMenu = new BurgerMenu();
        Assert.assertTrue("I am at the home page as a not logged user", burgerMenu.btn_Settings.getText().contains("Settings"));
        CommonActions.clickBackButton();

    }

    @When("^I go to the Login page$")
    public void I_go_to_the_login_page() throws Throwable {
        title = new TitleBar();
        title.btn_burgerMenu.click();
        burgerMenu = new BurgerMenu();
        burgerMenu.btn_Settings.click();

    }

    @And("^I make login with \"([^\"]*)\" and \"([^\"]*)\"$")
    public void I_make_login_with_and(String arg1, String arg2) throws Throwable {
        login = new LoginPage();
        login.makeLogin(arg1, arg2);

    }


    @And("^I make login with Facebook account$")
    public void I_make_login_with_Facebook_account() throws Throwable {
        login = new LoginPage();
        login.makeLoginFacebook();

    }

    @When("^I make logout$")
    public void I_make_logout() throws Throwable {
        title = new TitleBar();
        title.btn_burgerMenu.click();
        burgerMenu = new BurgerMenu();
        burgerMenu.btn_Settings.click();
        profile = new ProfilesSettingsPage();
        System.out.println("aaaaaaa");
        while (!profile.logoutButtonIsDisplayed())
            CommonActions.swipeUp();
        profile.btn_Logout.click();
    }

    @Then("^the account is not complete message is displayed$")
    public void the_account_is_not_complete_message_is_displayed() throws Throwable {
        deactivateUser = new DeactivateUserPopup();
        Assert.assertTrue("the account is not complete message is displayed$", deactivateUser.txt_message.isDisplayed());
    }

    @Given("^I dismiss the account is not complete message$")
    public void I_dismiss_the_account_is_not_complete_message() throws Throwable {
        deactivateUser = new DeactivateUserPopup();
        deactivateUser.btn_closePopup.click();
    }

    @When("^I go to the login page$")
    public void iGoToTheLoginPage() throws Throwable {
        input = new InputPage();
        input.gotoLogin();
    }

    @Then("^the invalidLogin popup is displayed$")
    public void theInvalidLoginPopupIsDisplayed() throws Throwable {
        invalidLoginPopup = new InvalidLoginPopup();
        Assert.assertTrue("the invalidLogin popup is displayed",invalidLoginPopup.invalidLoginIsDisplayed());
    }

    @Then("^I am at the login page$")
    public void iAmAtTheLoginPage() throws Throwable {
        login = new LoginPage();
        Assert.assertTrue("I am at the login page", login.loginButtonIsDisplayed());

    }
}
