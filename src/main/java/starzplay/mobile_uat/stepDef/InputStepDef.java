package starzplay.mobile_uat.stepDef;

import cucumber.api.java.en.Given;
import org.junit.Assert;
import starzplay.mobile_uat.pom.pages.InputPage;

public class InputStepDef {


    InputPage input;

    @Given("^I am at the input page$")
    public void iAmAtTheInputPage() throws Throwable {
        input = new InputPage();
        Assert.assertTrue("I am at the input page", input.isSignupDisplayed());
    }
}
