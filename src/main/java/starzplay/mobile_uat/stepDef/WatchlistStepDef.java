package starzplay.mobile_uat.stepDef;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import starzplay.mobile_uat.pom.components.menus.BurgerMenu;
import starzplay.mobile_uat.pom.pages.ContentInformationPage;
import starzplay.mobile_uat.pom.pages.HomePage;
import starzplay.mobile_uat.pom.pages.WatchlistActionsPage;
import starzplay.mobile_uat.pom.pages.WatchlistPage;
import starzplay.mobile_uat.utility.CommonActions;


public class WatchlistStepDef {

    HomePage home;
    ContentInformationPage contentInformation;
    BurgerMenu burgerMenu;
    WatchlistPage watchlist;
    WatchlistActionsPage watchlistActions;
    String contentAdded;
    String contentRemoved;
    String contentViewed;

    @When("^I add content to the watchlist$")
    public void I_add_content_to_the_watchlist() throws Throwable {
        home = new HomePage();
        home.img_HeroContent.click();
        contentInformation = new ContentInformationPage();
        while (!contentInformation.titleIsDisplayed())
            CommonActions.swipeUpShort();
        contentAdded = contentInformation.txt_Title.getText();
        while (!contentInformation.addWatchListButtonIsDisplayed())
            CommonActions.swipeDownShort();
        contentInformation.btn_addWatchlist.click();
        contentInformation.waitForAddContentMessage();
        contentInformation.btn_closeView.click();

    }

    @Then("^I could view the content in my watchlist$")
    public void I_could_view_the_content_in_my_watchlis() throws Throwable {
        I_go_to_the_Watchlist_category();
        watchlist = new WatchlistPage();
        watchlist.tab_watchlist.click();
        Assert.assertTrue("content found",watchlist.checkMoviesInWatchlist(contentAdded));

    }

    @When("^I remove content from the watchlist$")
    public void I_remove_content_from_the_watchlist() throws Throwable {
        I_go_to_the_Watchlist_category();
        watchlist = new WatchlistPage();
        watchlist.tab_watchlist.click();
        contentRemoved = watchlist.contentDisplayed.get(1).getText();
        watchlist.longPress(watchlist.contentDisplayed.get(1));
        watchlistActions = new WatchlistActionsPage();
        watchlistActions.btn_Remove.click();

    }

    @Then("^I could not view the content in my watchlist$")
    public void I_could_not_view_the_content_in_my_watchlist() throws Throwable {
        watchlist = new WatchlistPage();
        Assert.assertFalse("content not found", watchlist.checkMoviesInWatchlist(contentRemoved));

    }

    @And("^I am watching some content$")
    public void I_am_watching_some_content() throws Throwable {
        home = new HomePage();
        home.img_HeroContent.click();
        contentInformation = new ContentInformationPage();
        while (!contentInformation.titleIsDisplayed())
            CommonActions.swipeUpShort();
        contentViewed = contentInformation.txt_Title.getText();
        if (home.btn_watchNow.getText().equals("WATCH NOW")) {
            contentInformation.btn_watchNow.click();
            Thread.sleep(35000);
            CommonActions.clickBackButton();
        }
        contentInformation.btn_closeView.click();

    }

    @Then("^I could  view the content I am watching$")
    public void I_could_view_the_content_I_am_watching() throws Throwable {
        I_go_to_the_Watchlist_category();
        watchlist = new WatchlistPage();
        Assert.assertTrue("I could  view the content I am watching", watchlist.checkMoviesInWatchlist(contentViewed));

    }

    @Then("^I cannot add content into my watchlist$")
    public void I_cannot_add_content_into_my_watchlist() throws Throwable {
        home = new HomePage();
        Assert.assertTrue("I cannot add content into my watchlist", !home.addContentButtonIsDisplayed());

    }

    @Then("^I can add content into my watchlist$")
    public void I_can_add_content_into_my_watchlist() throws Throwable {
        home = new HomePage();
        Assert.assertTrue("I can add content into my watchlist", home.addContentButtonIsDisplayed());

    }

    @Then("^I go to the Watchlist category$")
    public void I_go_to_the_Watchlist_category() throws Throwable {
        home = new HomePage();
        home.btn_burgerMenu.click();
        burgerMenu = new BurgerMenu();
        burgerMenu.btn_Watchlist.click();

    }

    @And("^I am watching the content displayed$")
    public void I_am_watching_the_content_displayed() throws Throwable {
        home = new HomePage();
        home.img_HeroContent.click();
        if (home.btn_watchNow.getText().equals("WATCH NOW")) {
            contentInformation = new ContentInformationPage();
            contentInformation.btn_watchNow.click();
            Thread.sleep(20000);
            CommonActions.clickBackButton();

        }
        CommonActions.swipeRight();
        CommonActions.swipeLeft();
    }

    @And("^I add content to the watching list$")
    public void I_add_content_to_the_watching_list() throws Throwable {


    }
}
