package starzplay.mobile_uat.stepDef;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import starzplay.mobile_uat.pom.pages.ChromecastConnectionPage;
import starzplay.mobile_uat.pom.pages.HomePage;
import starzplay.mobile_uat.utility.CommonActions;

public class ChromecastStepDef {


    HomePage home;
    ChromecastConnectionPage chromecastConnect;

    @Then("^the chromecast icon is displayed$")
    public void the_chromecast_icon_is_displayed() throws Throwable {
        Assert.assertTrue("The chromecast icon is displayed$", CommonActions.chromecastButtonIsDisplayed());
    }

    @When("^the chromecast icon is not displayed$")
    public void the_chromecast_icon_is_not_displayed() throws Throwable {
        Assert.assertTrue("the chromecast icon is not displayed",!CommonActions.chromecastButtonIsDisplayed());
    }

    @When("^I create a chromecast connection$")
    public void I_create_a_chromecast_connection() throws Throwable {
        home = new HomePage();
        CommonActions.btn_Chromecast.click();
    }

    @Then("^the connect to device page is displayed$")
    public void the_connect_to_device_page_is_displayed() throws Throwable {
        chromecastConnect = new ChromecastConnectionPage();
        Assert.assertTrue("the connect to device page is displayed$",chromecastConnect.webEl_chromecastDevices.isDisplayed());
    }


}
