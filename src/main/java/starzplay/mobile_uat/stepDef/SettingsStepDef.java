package starzplay.mobile_uat.stepDef;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import starzplay.mobile_uat.pom.pages.*;
import starzplay.mobile_uat.utility.CommonActions;

import java.util.ArrayList;
import java.util.List;

public class SettingsStepDef {

    SettingsPage settings;
    SelectActionPage select;
    HelpPage help;
    ProfilesSettingsPage profile;
    ParentalSettingsPage parental;
    DevicesSettingsPage devices;
    String email = "automoba@a.com";

    @Then("^I am at the Settings page$")
    public void I_am_at_the_Settings_page() throws Throwable {
        settings = new SettingsPage();
        ProfilesSettingsPage profile = new ProfilesSettingsPage();
        Assert.assertTrue("I am at the Settings page", profile.txt_personalInf.isDisplayed());
    }

    @And("^I change my profile picture$")
    public void I_change_my_profile_picture() throws Throwable {
        profile = new ProfilesSettingsPage();
        profile.img_profilePicture.click();
        select = new SelectActionPage();
        select.mobElem_selectActionsApps.get(1).click();

    }

    @Then("^the picture is correctly displayed$")
    public void the_picture_is_correctly_displayed() throws Throwable {

    }

    @And("^I go to the parental control section$")
    public void I_go_to_the_parental_control_section() throws Throwable {
        settings = new SettingsPage();
        settings.getParental().click();

    }


    @And("^I have the parental control \"([^\"]*)\"$")
    public void I_have_the_parental_control(String arg1) throws Throwable {

    }

    @When("^I change the parental control to \"([^\"]*)\"$")
    public void I_change_the_parental_control_to(String arg1) throws Throwable {

    }

    @When("^I change the application language to \"([^\"]*)\"$")
    public void I_change_the_application_language_to(String arg1) throws Throwable {

    }

    @Then("^the application is displayed in \"([^\"]*)\"$")
    public void the_application_is_displayed_in(String arg1) throws Throwable {

    }

    @Then("^the help page is displayed$")
    public void the_help_page_is_displayed() throws Throwable {
        help =  new HelpPage();
    }

    @When("^I enter in the Devices page$")
    public void iEnterInTheDevicesPage() throws Throwable {
        settings = new SettingsPage();
       settings.getDevices().click();
        Thread.sleep(5000);
    }

    @Then("^The number of devices is showed$")
    public void theNumberOfDevicesIsShowed() throws Throwable {
        devices = new DevicesSettingsPage();
        int cont ;
        int j = 0;
        List<String> names = new ArrayList<String>();
        String elem;
        String elem2;
        boolean found = false;
        List<WebElement> list1 = devices.list_devices;
        for(int i = 0;i < list1.size();i++){
            elem2=list1.get(i).findElement(By.id("com.parsifal.starz:id/textViewDeviceNameFill")).getText();
            names.add(i,elem2);
        }
        cont = list1.size();
        CommonActions.swipeUp();
        List<WebElement> list2 =devices.list_devices;
        for(int i = 0; i < list2.size();i++){
            elem = list2.get(i).findElement(By.id("com.parsifal.starz:id/textViewDeviceNameFill")).getText();
            while(!found && j< list1.size()){
                if(elem.equalsIgnoreCase(names.get(j)))
                    found = true;
                else
                    j++;
            }
            if(!found)
                cont++;
            else {
                found = false;
                j=0;
            }
        }

    }

    @When("^I clear the first name field$")
    public void iClearTheFirstNameField() throws Throwable {
        profile = new ProfilesSettingsPage();
        profile.fld_userFirstName.clear();

    }

    @Then("^the message of required field is displayed$")
    public void theMessageOfRequiredFieldIsDisplayed() throws Throwable {

    }

    @When("^I clear the last name field$")
    public void iClearTheLastNameField() throws Throwable {
        profile = new ProfilesSettingsPage();
        profile.fld_userLastName.clear();
    }

    @When("^I go to the \"([^\"]*)\" section$")
    public void I_go_to_the_section(String arg1) throws Throwable {
        settings =  new SettingsPage();
        if (arg1.equalsIgnoreCase("Profiles"))
            settings.getProfile().click();
        else if (arg1.equalsIgnoreCase("Payments")) {
            settings.getPayments().click();
        }
        else if (arg1.equalsIgnoreCase("Devices"))
            settings.getDevices().click();
        else if (arg1.equalsIgnoreCase("Parental control"))
            settings.getParental().click();
    }

    @Then("^I am at the \"([^\"]*)\" section$")
    public void I_am_at_the_section(String arg1) throws Throwable {
        settings =  new SettingsPage();
        if (arg1.equalsIgnoreCase("Profiles")) {
            PaymentsSettingsPage payments = new PaymentsSettingsPage();
            Assert.assertTrue("I am at the " + arg1 + " section", payments.txt_AccountStatus.isDisplayed());
        }
        else if (arg1.equalsIgnoreCase("Payments"))
            settings.getPayments().click();
        else if (arg1.equalsIgnoreCase("Devices"))
            settings.getDevices().click();
        else if (arg1.equalsIgnoreCase("Parental control"))
            settings.getParental().click();

        Thread.sleep(3000);
    }

    @When("^I change the user email$")
    public void I_change_the_user_email() throws Throwable {
        profile = new ProfilesSettingsPage();
        profile.btn_editEmail.click();
        ChangeEmailProfilesSettingsPage changeEmail = new ChangeEmailProfilesSettingsPage();
        changeEmail.fld_currentEmail.sendKeys("automob@a.com");
        changeEmail.fld_newEmail.sendKeys(email);
        changeEmail.fld_confirmEmail.sendKeys(email);
        changeEmail.btn_OK.click();
    }

    @Then("^the request successful message is displayed$")
    public void the_request_successful_message_is_displayed() throws Throwable {
        RequestSuccessfulPage request = new RequestSuccessfulPage();
        Assert.assertTrue("the request successful message is displayed",request.txt_title.isDisplayed());
    }

    @When("^I change the user password$")
    public void I_change_the_user_password() throws Throwable {
        profile = new ProfilesSettingsPage();
        profile.btn_editPass.click();
        ChangePassProfilesSettingsPage changePass = new ChangePassProfilesSettingsPage();
        changePass.fld_currentPass.sendKeys("test123");
        changePass.fld_newPass.sendKeys("test123");
        changePass.fld_confirmPass.sendKeys("test123");
        changePass.btn_OK.click();
    }

    @When("^I change the parental setting$")
    public void I_change_the_parental_setting() throws Throwable {
        settings = new SettingsPage();
        settings.getParental().click();
        parental = new ParentalSettingsPage();
        parental.getG().click();
    }

}
