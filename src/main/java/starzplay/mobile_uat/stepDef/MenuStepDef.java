package starzplay.mobile_uat.stepDef;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import starzplay.mobile_uat.pom.components.menus.BurgerMenu;
import starzplay.mobile_uat.pom.pages.HomePage;
import starzplay.mobile_uat.pom.pages.SettingsPage;
import starzplay.mobile_uat.pom.pages.WatchlistPage;


public class MenuStepDef {

    HomePage home;
    BurgerMenu burgerMenu;
    WatchlistPage watchlist;
    SettingsPage settings;

    @Given("^I am at the home page$")
    public void I_am_at_the_home_page() throws Throwable {
        home = new HomePage();
        Assert.assertTrue("I_am_at_the_home_page",home.isLogged());
    }

    @Then("^I am at the burger menu page$")
    public void I_am_at_the_burger_menu_page() throws Throwable {
        burgerMenu = new BurgerMenu();
        Assert.assertTrue("",burgerMenu.elem_Categories.isDisplayed());
    }

    @And("^I go to the menu page$")
    public void I_go_to_the_menu_page() throws Throwable {
        home = new HomePage();
        home.openBurgerMenu();
    }

    @When("^I go to the help page$")
    public void I_go_to_the_help_page() throws Throwable {
        home = new HomePage();
        home.openBurgerMenu();
        burgerMenu = new BurgerMenu();
        burgerMenu.btn_Help.click();

    }

    @When("^I go to the Watchlist page$")
    public void I_go_to_the_Watchlist_page() throws Throwable {
        home = new HomePage();
        home.openBurgerMenu();
        burgerMenu = new BurgerMenu();
        burgerMenu.btn_Watchlist.click();
    }

    @Then("^I am at the Watchlist page$")
    public void I_am_at_the_Watchlist_page() throws Throwable {
        watchlist = new WatchlistPage();
        Assert.assertTrue("I am at the Watchlist page", watchlist.txt_pageTitle.getText().equals("WATCHLIST"));
    }

    @When("^I go to the Settings page$")
    public void I_go_to_the_Settings_page() throws Throwable {
        home = new HomePage();
        home.openBurgerMenu();
        burgerMenu = new BurgerMenu();
        burgerMenu.btn_Settings.click();
    }


    @When("^I go to the Signup page$")
    public void I_go_to_the_Signup_page() throws Throwable {
        home = new HomePage();
        home.openBurgerMenu();
        burgerMenu = new BurgerMenu();
        burgerMenu.btn_Signup.click();
    }
}
