package starzplay.mobile_uat.stepDef;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import starzplay.mobile_uat.pom.components.TitleBar;
import starzplay.mobile_uat.pom.components.menus.BurgerMenu;
import starzplay.mobile_uat.pom.pages.LoginPage;
import starzplay.mobile_uat.resources.Browsers.BrowserFactory;
import starzplay.mobile_uat.utility.CommonActions;

import java.net.MalformedURLException;

public class Hooks {

    @Before("@SetupDriver")
    public void setupAppiumConnectionHook() throws MalformedURLException {
        BrowserFactory.getBrowser(System.getProperty("browser"));
        CommonActions.resetApp();
    }

    @After("@SetupDriver")
    public void tearDownAppiumConnectionHook(Scenario scenario) throws MalformedURLException {
        if (scenario.isFailed()) {
            CommonActions.embedScreenshot(scenario);
        }
        BrowserFactory.closeBrowser();
    }

    @Before("@MakeLogin_ActiveUser")
    public void makeLoginActiveUser() throws Throwable {
        TitleBar title = new TitleBar();
        title.btn_burgerMenu.click();
        BurgerMenu burger = new BurgerMenu();
        burger.btn_Settings.click();
        LoginPage login =  new LoginPage();
        login.makeLogin("automob@playco.com", "test123");
    }



}


