package starzplay.mobile_uat.stepDef;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import starzplay.mobile_uat.pom.pages.AccountCreationPage;
import starzplay.mobile_uat.pom.pages.AccountValidationPage;

import java.util.Date;

public class OpenSignupStepDef {

    AccountCreationPage accountCreation;
    AccountValidationPage accountValidation;

    @Then("^I am at the account creation page$")
    public void I_am_at_the_account_creation_page() throws Throwable {
        accountCreation = new AccountCreationPage();
        Assert.assertTrue("I am at the account creation page",accountCreation.txt_PromoDesc.isDisplayed());

    }

    @And("^I start the signup process$")
    public void I_start_the_signup_process() throws Throwable {
    }

    @When("^I dismiss the account creation page$")
    public void I_dismiss_the_account_creation_page() throws Throwable {
        accountCreation = new AccountCreationPage();
        accountCreation.btn_Back.click();
    }

    @When("^I dismiss the account validation page$")
    public void I_dismiss_the_account_validation_page() throws Throwable {
        accountValidation = new AccountValidationPage();
        accountCreation.btn_Back.click();
    }

    @When("^I go to the account validation page$")
    public void I_go_to_the_validation_page() throws Throwable {
        accountCreation = new AccountCreationPage();
        accountCreation.btn_Continue.click();
    }

    @Then("^the \"([^\"]*)\" error message is displayed$")
    public void the_error_message_is_displayed(String arg1) throws Throwable {

    }

    @Then("^I am at the validation page$")
    public void I_am_at_the_validation_page() throws Throwable {
        // Express the Regexp above with the code you wish you had
        throw new PendingException();
    }

    @When("^I enter the user first name \"([^\"]*)\"$")
    public void I_enter_the_user_first_name(String arg1) throws Throwable {
        accountCreation = new AccountCreationPage();
        accountCreation.fld_firstName.sendKeys(arg1);
    }

    @And("^I enter the user last name \"([^\"]*)\"$")
    public void I_enter_the_user_last_name(String arg1) throws Throwable {
        accountCreation = new AccountCreationPage();
        accountCreation.fld_lastName.sendKeys(arg1);
    }

    @And("^I enter the user email \"([^\"]*)\"$")
    public void I_enter_the_user_email(String arg1) throws Throwable {
        accountCreation = new AccountCreationPage();
        accountCreation.fld_email.sendKeys(arg1);
    }

    @And("^I enter the user confirm email \"([^\"]*)\"$")
    public void I_enter_the_user_confirm_email(String arg1) throws Throwable {
        accountCreation = new AccountCreationPage();
        accountCreation.fld_confirmEmail.sendKeys(arg1);
    }

    @And("^I enter the user password \"([^\"]*)\"$")
    public void I_enter_the_user_password(String arg1) throws Throwable {
        accountCreation = new AccountCreationPage();
        accountCreation.fld_password.sendKeys(arg1);
    }

    @Then("^the already exist starzplay account is displayed$")
    public void the_already_exist_starzplay_account_is_displayed() throws Throwable {
    }

    @And("^I enter user details for open signup process$")
    public void I_enter_user_details_for_open_signup_process() throws Throwable {
        Date date = new Date();
        accountCreation = new AccountCreationPage();
        accountCreation.fld_firstName.sendKeys("test_firstName");
        accountCreation.fld_lastName.sendKeys("test_lastName");
        accountCreation.fld_email.sendKeys(date + "@a.com");
        accountCreation.fld_confirmEmail.sendKeys(date + "@a.com");
        accountCreation.fld_password.sendKeys("test123");
    }

    @When("^I enter user details for validation account signup process$")
    public void I_enter_user_details_for_validation_account_signup_process() throws Throwable {
        accountValidation = new AccountValidationPage();
        accountValidation.fld_mobileNumber.sendKeys("111111119");
    }

    @And("^I go to the validation second step page$")
    public void I_go_to_the_validation_second_step_page() throws Throwable {
        accountValidation = new AccountValidationPage();
        accountValidation.btn_Continue.click();
    }

    @Then("^I am at the account validation second step page$")
    public void I_am_at_the_the_validation_second_step_page() throws Throwable {
        accountCreation = new AccountCreationPage();
    }

    @Given("^I am at the account validation page$")
    public void I_am_at_the_account_validation_page() throws Throwable {
        accountCreation = new AccountCreationPage();
    }

    @Then("^the warning message is displayed$")
    public void the_warning_message_is_displayed() throws Throwable {

    }
}
