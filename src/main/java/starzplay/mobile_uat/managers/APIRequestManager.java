package starzplay.mobile_uat.managers;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import starzplay.mobile_uat.resources.Operators.Operator;
import starzplay.mobile_uat.resources.Operators.OperatorFactory;
import starzplay.mobile_uat.resources.Users.User;
import starzplay.mobile_uat.utility.Constant;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import static org.apache.http.protocol.HTTP.USER_AGENT;
import static starzplay.mobile_uat.managers.DBConnectionManager.getVerificationCodeFromDB;


public class APIRequestManager {

    private static String tractUserName = "david.sanchez.tennant6";
    private static String tractPassword = "tester01!";
    private static String esbUsername = "Starz";
    private static String esbPassword = "St@rzPr!@";
    //private static String passValid = "test123";
    private static String voucherValid = "12PL1501ESB01";

    private static JSONObject sendGet(String endpoint, Boolean params, String query, Boolean tract, String userName, String password, String accessToken) throws Exception {
        URL url;
        if (params) {
            url = new URL(endpoint + "?" + query);
        } else {
            url = new URL(endpoint);
        }
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        if (tract) {
            String authString = userName + ":" + password;
            byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            con.setRequestProperty("Authorization", "Basic " + authStringEnc);
        } else {
            con.setRequestProperty("Authorization", accessToken);
        }

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONObject xmlJSONObj;

        if (tract) {
            xmlJSONObj = XML.toJSONObject(response.toString());
        } else {
            xmlJSONObj = new JSONObject(response.toString());
        }

        return xmlJSONObj;
    }

    private static String readAll(Reader rd) {
        StringBuilder sb = new StringBuilder();
        int cp;
        try {
            while ((cp = rd.read()) != -1) {
                sb.append((char) cp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String urlString) {
        BufferedReader reader = null;
        StringBuffer buffer = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

        } catch (IOException e) {
            e.printStackTrace();
        }
        //reader.close();
        return new JSONObject(buffer.toString());

    }

    public static Map<String, String> parse(JSONObject json, Map<String, String> out) throws JSONException {
        Iterator<String> keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            String val = null;
            try {
                JSONObject value = json.getJSONObject(key);
                parse(value, out);
            } catch (Exception e) {
                val = json.getString(key);
            }

            if (val != null) {
                out.put(key, val);
            }
        }
        return out;
    }


    public static void sendPut(String endpoint, String accessToken, String body, Boolean transaction, String transactionID) throws Exception {
        URL url = new URL(endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("PUT");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        //connection.setRequestProperty("Authorization", accessToken);
        if (transaction)
            connection.setRequestProperty("smsTransactionId", transactionID);
        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
        System.out.println(connection.getOutputStream());

        osw.flush();
        osw.close();
        System.out.println("response code " + connection.getResponseCode());
        connection.getResponseCode();
    }

    public static void sendGet(String endpoint) throws Exception {

        URL url = new URL(endpoint);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
//
//        int responseCode = con.getResponseCode();
//        System.out.println("\nSending 'GET' request to URL : " + url);
//        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
    }

    private static JSONObject sendPost(String endpoint, String accessToken, String body) throws Exception {
        URL url = new URL(endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("Authorization", accessToken);
        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
        osw.write(body);
        osw.flush();
        osw.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONObject xmlJSONObj;
        xmlJSONObj = new JSONObject(response.toString());
        connection.getResponseCode();
        return xmlJSONObj;
    }

    private static JSONObject sendPostESBPublic(String endpoint, String body, Boolean apikey, String accessToken) throws Exception {
        URL url = new URL(endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        if (apikey)
            connection.setRequestProperty("API-KEY", "04b616fb3c6af49de293ad0a5f6be34498fb6fd6");
        if (accessToken != null && !accessToken.isEmpty())
            connection.setRequestProperty("Authorization", accessToken);
        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
        osw.write(body);
        osw.flush();
        osw.close();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONObject xmlJSONObj;
        xmlJSONObj = new JSONObject(response.toString());
        connection.getResponseCode();
        return xmlJSONObj;
    }

    public static JSONObject sendPostMSISDN(String endpoint, String accessToken, String body) throws Exception {
        URL url = new URL(endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        connection.setRequestProperty("smsTransactionId", accessToken);
        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
        osw.write(body);
        osw.flush();
        osw.close();
        System.out.println(connection.getResponseCode());

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONObject xmlJSONObj;
        xmlJSONObj = new JSONObject(response.toString());
        connection.getResponseCode();
        System.out.println(xmlJSONObj);
        return xmlJSONObj;

    }

    public static JSONObject sendPutMSISDN(String endpoint, String body) throws Exception {
        URL url = new URL(endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("PUT");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
        osw.write(body);
        osw.flush();
        osw.close();
        System.out.println(connection.getResponseCode());

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONObject xmlJSONObj;
        xmlJSONObj = new JSONObject(response.toString());
        connection.getResponseCode();
        System.out.println(response);
        return xmlJSONObj;

    }

    private static JSONObject sendPostESBPrivate(String endpoint, String body, Boolean needsPassword) throws Exception {
        URL url = new URL(endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        String authString = esbUsername + ":" + esbPassword;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        connection.setRequestProperty("Authorization", "Basic " + authStringEnc);

        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        if (needsPassword) {
            connection.setRequestProperty("password", Constant.password);
        }
        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
        osw.write(body);
        osw.flush();
        osw.close();
        connection.getResponseCode();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        JSONObject xmlJSONObj;
        xmlJSONObj = new JSONObject(response.toString());
        connection.getResponseCode();
        return xmlJSONObj;
    }

    private static void sendPostDisconnect(String endpoint, String body, Boolean needsPassword) throws Exception {
        URL url = new URL(endpoint);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        String authString = esbUsername + ":" + esbPassword;
        byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
        String authStringEnc = new String(authEncBytes);
        connection.setRequestProperty("Authorization", "Basic " + authStringEnc);

        connection.setRequestMethod("POST");
        connection.setDoOutput(true);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Accept", "application/json");
        if (needsPassword) {
            connection.setRequestProperty("password", Constant.password);
        }
        OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
        osw.write(body);
        osw.flush();
        osw.close();
        connection.getResponseCode();
    }

    public static String getUID(String email) {
        try {
            JSONObject response = APIRequestManager.sendGet("https://parsifal.tract-staging.com/t/s/r/1.28/billingAccounts", true,
                    "emailAddress=" + email, true, tractUserName, tractPassword, "");
            return response.getJSONObject("billingAccounts").getJSONObject("ns2:billingAccount").getString("externalAccountNum");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }

    public static String getTractServiceCurrent(String uid) {
        try {
            JSONObject response = APIRequestManager.sendGet("https://parsifal.tract-staging.com/t/s/r/1.28/services", true,
                    "status=SERVICE_ACTIVE&externalAccountNum=" + "678253db208b493e9aa5bf6492234c0d" + "&queryScope=DEEP", true, tractUserName, tractPassword, "");
            return response.getJSONObject("services").getJSONObject("ns2:service").getJSONObject("ns2:product").getString("name");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }


    public static String getTractServiceNext(String uid) {
        try {
            JSONObject response = APIRequestManager.sendGet("https://parsifal.tract-staging.com/t/s/r/1.28/services", true,
                    "status=SERVICE_ACTIVE&externalAccountNum=" + uid + "&queryScope=DEEP", true, tractUserName, tractPassword, "");
            return response.getJSONObject("services")
                    .getJSONObject("ns2:service")
                    .getJSONObject("ns2:currentAgreementService")
                    .getJSONObject("ns2:nextServiceProduct")
                    .getString("name");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }

    public static LoginData esbLogin(String email, String password) {
        String env = "tst";

        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            env = "dev";
        }

        try {
            JSONObject response = APIRequestManager.sendGet("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/login", true,
                    "email=" + email + "&password=" + password, false, "", "", "");
            return new LoginData(response.getString("uid"), response.getString("accessToken"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new LoginData("0", "0");
    }

    public static void changeEmailAddress(LoginData loginData, String email) {
        String env = "tst";

        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            env = "dev";
        }

        String body = "{\"emailAddress\": \"" + email + "\"}";
        try {
            APIRequestManager.sendPut("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts/" + loginData.uid,
                    loginData.accessToken, body, false, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void changePassword(LoginData loginData, String email) {
        String env = "tst";

        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            env = "dev";
        }

        String body = "{\"password\": \"" + email + "\"}";
        try {
            APIRequestManager.sendPut("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts/" + loginData.uid,
                    loginData.accessToken, body, false, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String requestStatus(LoginData loginData) {
        String env = "tst";

        JSONObject response = null;

        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            env = "dev";
        }

        try {
            response = APIRequestManager.sendGet("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts/" + loginData.uid,
                    false, "", false, "", "", loginData.accessToken);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            return response.getJSONObject("settings").getString("accountStatus");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "0";
    }

    public static String  requestVerificationCode(LoginData loginData, String type, String phoneValid) {
        String env = "tst";
        String body = "";

        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            env = "dev";
        }

        body = "{\"destination\":" + phoneValid + "}";

        try {
            JSONObject response = APIRequestManager.sendPost("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts/" + loginData.uid
                    + "/requestVerification/" + type + "?repeat=false", loginData.accessToken, body);
            return response.getString("smsTransactionId");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }

    public static void validateVerificationCode(LoginData loginData, String type, String transactionID, String code) {
        String env = "tst";
        String body = "";

        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            env = "dev";
        }

        try {
            APIRequestManager.sendPut("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts/" + loginData.uid
                    + "/validateVerificationCode/" + type + "/" + code, loginData.accessToken, body, true, transactionID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static User createNewActiveUser(String MSISDN, String payment, Boolean weekly) throws Exception {
        String env = "tst";
        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            env = "dev";
        }
        String username = "";
        String body;

        String paymentMethod = null;
        String ddbb;
        LoginData credentials = null;
        String billingPhone = null;

        if (MSISDN !=null && !MSISDN.isEmpty()) {
            Operator operator = OperatorFactory.getOperator(MSISDN);

            if (payment.equalsIgnoreCase("Du") ||
                    payment.equalsIgnoreCase("Etisalat") ||
                    payment.equalsIgnoreCase("Ooredoo") ||
                    payment.equalsIgnoreCase("OoredooKuwait") ||
                    payment.equalsIgnoreCase("Vodafone") ||
                    payment.equalsIgnoreCase("Viva") ||
                    payment.equalsIgnoreCase("Orange") ||
                    payment.equalsIgnoreCase("STC")) {

                APIClientGigya.deleteUserData(operator);
                username = operator.getCountryPrefix() + operator.getValidTelephoneNumber();

                if (weekly)
                    paymentMethod = operator.getPaymentName() + "_week";
                else
                    paymentMethod = operator.getPaymentName();

                body = "{\n" +
                        "\t\"userIdentifier\":\"" + username + "\", \n" +
                        "\t\"termsAndConditions\":\"true\",\n" +
                        "\t\"password\":\"" + Constant.password + "\", \n" +
                        "\"settings\": {\"" +
                        "CampaignID\": \"\", \"" +
                        "language\": \"en\", \"" +
                        "countrySignUp\": \"IE\", \"" +
                        "countryUser\": \"IE\"" +
                        "}}";

                try {
                    APIRequestManager.sendPostESBPublic("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts?paymentMethod=" + paymentMethod, body, true, null);

                } catch (Exception e) {
                    System.out.println("API error creating MSISDN user");
                    e.printStackTrace();
                }
            }else if (payment.equalsIgnoreCase("creditcard")) {

                username = operator.getCountryPrefix() + operator.getValidTelephoneNumber();

                body = "{\n" +
                        "\t\"userIdentifier\":\"" + username + "\", \n" +
                        "\t\"termsAndConditions\":\"true\",\n" +
                        "\t\"password\":\"" + Constant.password + "\", \n" +
                        "\"settings\": {\"" +
                        "CampaignID\": \"\", \"" +
                        "language\": \"en\", \"" +
                        "countrySignUp\": \"IE\", \"" +
                        "countryUser\": \"IE\"" +
                        "}}";

                try {
                    APIRequestManager.sendPostESBPublic("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts", body, true, null);
                } catch (Exception e) {
                    System.out.println("API error creating prospect MSISDN user");
                    e.printStackTrace();
                }
                try {
                    credentials = APIRequestManager.esbLogin(username, Constant.password);
                } catch (Exception e) {
                    System.out.println("API error esbLogin");
                    e.printStackTrace();
                }
                String bodyCreditCard = "{\n" +
                        "\"paymentMethods\": [{\"" +
                        "paymentType\": \"credit card\", \"" +
                        "creditCardType\": \"VISA\", \"" +
                        "creditCardNumber\": \"4111111111111111\", \"" +
                        "creditCardFirstName\": \"Test\", \"" +
                        "creditCardLastName\": \"Test\", \"" +
                        "creditCardExpiration\": \"12/2024\", \"" +
                        "creditCardVerificationNumber\": \"123\"" +
                        "}],\n" +
                        "\"subscriptions\": [{\"" +
                        "type\": \"SVOD Trial\", \"" +
                        "priceDiscount\": \"\"" +
                        "}]}";
                try {
                    if (credentials != null) {
                        APIRequestManager.sendPostESBPublic("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts/" + credentials.uid + "/billingAccounts", bodyCreditCard, false, credentials.accessToken);
                    }
                } catch (Exception e) {
                    System.out.println("API error putting creditcard payment method to MSISDN user");
                    e.printStackTrace();
                }
            }
            return new User(username, Constant.password, null);
        } else {

            username = "NewEmailActiveUser" + System.currentTimeMillis() + "@starzplayarabia.com";

            body = "{\n" +
                    "\t\"userIdentifier\":\"" + username + "\", \n" +
                    "\t\"termsAndConditions\":\"true\",\n" +
                    "\t\"confirmEmail\":\"" + username + "\", \n" +
                    "\t\"password\":\"" + Constant.password + "\", \n" +
                    "\"settings\": {\"" +
                    "CampaignID\": \"\", \"" +
                    "language\": \"en\", \"" +
                    "countrySignUp\": \"IE\", \"" +
                    "countryUser\": \"IE\"" +
                    "}}";

            try {
                APIRequestManager.sendPostESBPublic("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts", body, true, null);
                credentials = APIRequestManager.esbLogin(username, Constant.password);
            } catch (Exception e) {
                System.out.println("API error creating email prospect user");
                e.printStackTrace();
            }

            if (payment.equalsIgnoreCase("Du") ||
                    payment.equalsIgnoreCase("Etisalat") ||
                    payment.equalsIgnoreCase("Ooredoo") ||
                    payment.equalsIgnoreCase("OoredooKuwait") ||
                    payment.equalsIgnoreCase("Vodafone") ||
                    payment.equalsIgnoreCase("Viva") ||
                    payment.equalsIgnoreCase("Orange") ||
                    payment.equalsIgnoreCase("STC")) {

                Operator operator = OperatorFactory.getOperator(payment);

                if (weekly) {
                    ddbb = operator.getDdbbWeeklyCode();
                }
                else {
                    ddbb = operator.getDdbbMonthlyCode();
                }

                try {
                    billingPhone = operator.getCountryPrefix() + operator.getValidTelephoneNumber();

                    String transactionID = requestVerificationCode(credentials,ddbb,billingPhone);
                    String code = getVerificationCodeFromDB(billingPhone,true);
                    validateVerificationCode(credentials,ddbb,transactionID,code);

                } catch (Exception e) {
                    System.out.println("API error creating email user with billing phone");
                    e.printStackTrace();
                }

                return new User(username, Constant.password,billingPhone);

            } else if (payment.equalsIgnoreCase("creditcard")) {


                String bodyCreditCard = "{\n" +
                        "\"paymentMethods\": [{\"" +
                        "paymentType\": \"credit card\", \"" +
                        "creditCardType\": \"VISA\", \"" +
                        "creditCardNumber\": \"4111111111111111\", \"" +
                        "creditCardFirstName\": \"Test\", \"" +
                        "creditCardLastName\": \"Test\", \"" +
                        "creditCardExpiration\": \"12/2024\", \"" +
                        "creditCardVerificationNumber\": \"123\"" +
                        "}],\n" +
                        "\"subscriptions\": [{\"" +
                        "type\": \"SVOD Trial\", \"" +
                        "priceDiscount\": \"\"" +
                        "}]}";

                try {
                    if (credentials != null) {
                        APIRequestManager.sendPostESBPublic("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts/"
                                + credentials.uid + "/billingAccounts", bodyCreditCard, false, credentials.accessToken);
                    }
                } catch (Exception e) {
                    System.out.println("API error creating email user with credit card");
                    e.printStackTrace();
                }
                return new User(username, Constant.password,null);
            }
        }

        return null;
    }



    public static User createNewProspectUser(Boolean MSISDN) throws Exception {
        String env = "tst";
        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            env = "dev";
        }
        String username ="";
        String body;

        if (MSISDN) {

            String number = (""+System.currentTimeMillis()).substring(7);
            username = "97337" + number;

            body = "{\n" +
                    "\t\"userIdentifier\":\"" + username + "\", \n" +
                    "\t\"termsAndConditions\":\"true\",\n" +
                    "\t\"password\":\"" + Constant.password + "\", \n" +
                    "\"settings\": {\"" +
                    "CampaignID\": \"\", \"" +
                    "language\": \"en\", \"" +
                    "countrySignUp\": \"IE\", \"" +
                    "countryUser\": \"IE\"" +
                    "}}";


            try {
                APIRequestManager.sendPostESBPublic("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts", body, true, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {

            username = "NewEmailProspectUser" + System.currentTimeMillis() + "@starzplayarabia.com";

            body = "{\n" +
                    "\t\"userIdentifier\":\"" + username + "\", \n" +
                    "\t\"termsAndConditions\":\"true\",\n" +
                    "\t\"confirmEmail\":\"" + username + "\", \n" +
                    "\t\"password\":\"" + Constant.password + "\", \n" +
                    "\"settings\": {\"" +
                    "CampaignID\": \"\", \"" +
                    "language\": \"en\", \"" +
                    "countrySignUp\": \"IE\", \"" +
                    "countryUser\": \"IE\"" +
                    "}}";

            try {
                APIRequestManager.sendPostESBPublic("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts", body, true, null);

            } catch (Exception e) {
                System.out.println("MSISDN");
                e.printStackTrace();
            }
        }


        return new User(username, Constant.password, null);
    }



    public static User createNewUser(String partner,boolean MSISDN,String status,boolean email,boolean facebook) {
/*        String mail ="";
        String env = "tst";
        String mobile ="";
        User user;
        if(facebook && !email) {
            mobile = Constant.FacebookMobile;
        }
        else{
            if (partner.equalsIgnoreCase("Viva_bh")) {
                mobile = ("" + System.currentTimeMillis()).substring(7);
                mobile = "97337" + mobile;
            } else if (partner.equalsIgnoreCase("Du_uae")) {
                mobile = "971529752712";
            } else if (partner.equalsIgnoreCase("Etisalat_uae")) {
                mobile = ("" + System.currentTimeMillis()).substring(6);
                mobile = "97152" + mobile;
            } else if (partner.equalsIgnoreCase("Orange_jordan")) {
                mobile = "962779890199";
            } else if (partner.equalsIgnoreCase("Ooredoo_Tunisia")) {
                mobile = "21622170561";
            }
        }

        if(email){
            if(facebook){
                mail = Constant.FacebookEmail;
            }else{
                mail = "api_user" + System.currentTimeMillis() + "@starzplayarabia.com";
            }
        }

        try {
            if(MSISDN) {
                if (System.getProperty("environment").equalsIgnoreCase("dev")) {
                    env = "dev";
                }

                String body = "{\n" +
                        "\t\"channel\":\"\", \n" +
                        "\t\"parameter\":\"\"\n" +
                        "}";

                JSONObject response = APIRequestManager.sendPostESBPrivate("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/mobile/verify/" + mobile + "/signup?repeat=false",
                        body, true);
                String sms = response.getString("smsTransactionId");
                body = "{" +
                        "   \"userIdentifier\": \"" + mobile + "\",\n" +
                        "   \"termsAndConditions\": true,\n" +
                        "   \"password\": \"tester01.\",\n" +
                        "   \"settings\":    {\n" +
                        "      \"CampaignID\": \"\",\n" +
                        "      \"language\": \"en\",\n" +
                        "      \"countrySignUp\": \"IE\",\n" +
                        "      \"countryUser\": \"IE\"\n" +
                        "   }\n" +
                        "}";

                String code = getVerificationCodeFromDB("", mobile, "signup");
                System.out.println(code);
                if (status.equalsIgnoreCase("Active")) {
                    APIRequestManager.sendPostMSISDN("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts?otp=" + code + "&paymentMethod=" + partner, sms, body);
                } else if(status.equalsIgnoreCase("Prospect")) {
                    APIRequestManager.sendPostMSISDN("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts?otp=" + code, sms, body);
                } else if(status.equalsIgnoreCase("Disconnected")){
                    response = APIRequestManager.sendPostMSISDN("https://peg-" + env + "-public-api.eu.cloudhub.io/api/v0.2/userAccounts?otp=" + code  + "&paymentMethod=" + partner, sms, body);
                    body="{\n" +
                            "    \"settings\": {\n" +
                            "      \"requestedState\": \"Disconnected\",\n" +
                            "      \"deactivationReason\": \"Testing\"\n" +
                            "    }\n" +
                            "}";
                    APIRequestManager.sendPostDisconnect("https://test-esb-dmz-lb.aws.playco.com/sync/api/v0.1/deactivateService/"+response.get("globalUserId")+"?unconsistentData=false",body,false);
                }

                if (email) {
                    changeEmailAddress(esbLogin(mobile, Constant.password), mail);
                    user = new User(mail, Constant.password);
                }else
                    user = new User(mobile, Constant.password);
            }else{
                env="test";
                if (System.getProperty("environment").equalsIgnoreCase("dev")) {
                    env = "dev";
                }
                String body = "{\"target\": \"browser\", " +
                        "\"email\": \"" + mail + "\", \"confirmEmail\": \"" + mail + "\", \"termsAndConditions\": true," +
                        "\"settings\": {\"CampaignID\": \"\", \"language\": \"en\", \"countrySignUp\": \"IE\", \"countryUser\": \"IE\"" +
                        "}}";
                if (status.equalsIgnoreCase("Active")) {
                    APIRequestManager.sendPostESBPrivate("https://" + env + "-esb-dmz-lb.aws.playco.com/sync/api/v0.1/SignUp?voucher=" + voucherValid + "",
                            body, true);
                } else if(status.equalsIgnoreCase("Prospect")) {
                    APIRequestManager.sendPostESBPrivate("https://" + env + "-esb-dmz-lb.aws.playco.com/sync/api/v0.1/SignUp", body, true);
                }else if(status.equalsIgnoreCase("Disconnected")){
                    JSONObject response = APIRequestManager.sendPostESBPrivate("https://" + env + "-esb-dmz-lb.aws.playco.com/sync/api/v0.1/SignUp?voucher=" + voucherValid + "",
                            body, true);
                    body="{\n" +
                            "    \"settings\": {\n" +
                            "      \"requestedState\": \"Disconnected\",\n" +
                            "      \"deactivationReason\": \"Testing\"\n" +
                            "    }\n" +
                            "}";
                    APIRequestManager.sendPostDisconnect("https://test-esb-dmz-lb.aws.playco.com/sync/api/v0.1/deactivateService/"+response.get("uid")+"?unconsistentData=false",body,false);
                }

                user = new User(mail, Constant.password);
            }

            return user;
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        return null;
    }

    public static class LoginData {
        public String uid;
        public String accessToken;

        public LoginData(String uid, String accessToken) {
            this.uid = uid;
            this.accessToken = "Bearer " + accessToken;
        }
    }


    public static String getLocalPrice(String operator,Boolean weekly) {
        JSONObject json = APIRequestManager.readJsonFromUrl("https://peg-dev-public-api.eu.cloudhub.io/api/v0.2/configuration/paymentPlans?paymentMethod=" + operator);
        JSONObject jsonObject;
        JSONArray paymentPlans = json.getJSONArray("paymentPlans");
        if (weekly)
            jsonObject = paymentPlans.getJSONObject(0);
        else
            jsonObject = paymentPlans.getJSONObject(1);
        JSONArray paymentMethods = jsonObject.getJSONArray("paymentMethods");
        JSONObject jsonObject2 = paymentMethods.getJSONObject(0);
        JSONObject product = jsonObject2.getJSONObject("product");
        int localPrice = product.getInt("localPrice");

        return Integer.toString(localPrice);
    }

    public static String getLocalCurrency(String operator) {
        JSONObject json = APIRequestManager.readJsonFromUrl("https://peg-dev-public-api.eu.cloudhub.io/api/v0.2/configuration/paymentPlans?paymentMethod=" + operator);
        JSONArray paymentPlans = json.getJSONArray("paymentPlans");
        JSONObject jsonObject = paymentPlans.getJSONObject(0);
        JSONArray paymentMethods = jsonObject.getJSONArray("paymentMethods");
        JSONObject jsonObject2 = paymentMethods.getJSONObject(0);
        JSONObject product = jsonObject2.getJSONObject("product");
        String localCurrency = product.getString("localCurrency");

        return localCurrency;
    }

}
