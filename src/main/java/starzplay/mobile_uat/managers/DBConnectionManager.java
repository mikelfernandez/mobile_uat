package starzplay.mobile_uat.managers;

import starzplay.mobile_uat.resources.Operators.Operator;
import starzplay.mobile_uat.resources.Operators.OperatorFactory;
import starzplay.mobile_uat.resources.Users.User;

import java.sql.*;

import static starzplay.mobile_uat.pom.BasePage.driver;

public class DBConnectionManager {
    public static ResultSet executeQuery(String query) {

        String db = "staging";

        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
            db = "dev";
        }

        String connectionString = "jdbc:mysql://mysql-staging.aws.playco.com:3306/starzplayarabia_" + db;
        String dbClass = "com.mysql.jdbc.Driver";
        try {
            Class.forName(dbClass).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection con = null;
        try {
            con = DriverManager.getConnection(connectionString, "ec2_user", "PlayCo1234");
        } catch (SQLException e) {
            System.out.println("Couldn't connect to the database");
            e.printStackTrace();
        }
        Statement stmt = null;
        try {
            if (con != null) {
                stmt = (Statement) con.createStatement();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        ResultSet result = null;
        try {
            if (stmt != null) {
                System.out.println(query);
                result = (ResultSet) stmt.executeQuery(query);
            }
        } catch (SQLException e) {
            System.out.println("Coulnd't get the result");
            e.printStackTrace();
        }
        return result;
    }

    public static void updateQuery(String query) {

        String db = "staging";

//        if (System.getProperty("environment").equalsIgnoreCase("dev")) {
//            db = "dev";
//        }

        String connectionString = "jdbc:mysql://mysql-staging.aws.playco.com:3306/starzplayarabia_" + db;
        String dbClass = "com.mysql.jdbc.Driver";
        try {
            Class.forName(dbClass).newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection con = null;
        try {
            con = DriverManager.getConnection(connectionString, "ec2_user", "PlayCo1234");
        } catch (SQLException e) {
            System.out.println("Couldn't connect to the database");
            e.printStackTrace();
        }
        Statement stmt = null;
        try {
            if (con != null) {
                stmt = (Statement) con.createStatement();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            if (stmt != null) {
                System.out.println(query);
                stmt.executeUpdate(query);
            }
        } catch (SQLException e) {
            System.out.println("Coulnd't get the result");
            e.printStackTrace();
        }

    }
    public static String getVerificationCodeFromDB(String partner, Boolean signup) {
        Operator operator = OperatorFactory.getOperator(partner);
        ResultSet result;
        String type;
        String number;

        if (User.getUserPhone() != null && !User.getUserPhone().isEmpty())
            number =  User.getUserPhone();
        else
            number =  User.getUserName();

        if(signup)
            type = "signup";
        else {
            if (driver.findElementByXPath("//*[contains(@id,'main-plan-checkbox')]").isSelected())
                type = "mob_oper_" + operator.getPaymentName() + "_week";
            else
                type = "mob_oper_" + operator.getPaymentName();
        }
        result = DBConnectionManager.executeQuery("select `verification_code` from `sms_verification_codes` where `phone_number` = '"
                + number + "' and `is_valid` = '1' and `type` = '" + type + "' order by `id` DESC LIMIT 1");

        String verification_code = "";

        try {
            if (result.next()) {
                verification_code = result.getString(1);
            }
        } catch (SQLException e) {
            System.out.println("There was a problem executing the query");
            e.printStackTrace();
        }
        return verification_code;
    }


}