package starzplay.mobile_uat.managers;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;


/**
 * Created by Alexandre on 01/06/2016.
 */
public class ImageChecker {

    public static boolean getImages(String fileNameA, String fileNameB) throws FileNotFoundException, IOException{
        System.out.println("ImageA: " + fileNameA + "\n ImageB: " + fileNameB);
        return compareImage(copyFile(fileNameA),copyFile(fileNameB));
    }

    public static File copyFile(String imageUrl) {
        File file =  null;
        if (!imageUrl.contains(System.getProperty("user.dir"))) {
            try {
                URL url = new URL(imageUrl);
                URLConnection urlCon = url.openConnection();
                //read the file
                InputStream is = urlCon.getInputStream();
                file = createFileFromUrl(url.getPath());
                FileOutputStream fos = new FileOutputStream(file.getPath());
                byte[] array = new byte[1000];
                int readed = is.read(array);
                System.out.print("Start coping file: ");
                while (readed > 0) {
                    System.out.print(".");
                    fos.write(array, 0, readed);
                    readed = is.read(array);
                }
                System.out.println(" ");
                fos.close();
                return file;
            }catch(Exception e){
                e.printStackTrace();
            }
        }else  file = new File (imageUrl);
        return file;
    }

    public static File createFileFromUrl(String urlPath){
        String userHome = System.getProperty("user.home");
        String folder = "src/test/resources/tmpDriveImages/tmpImages";
        File fileFolder = new File(folder);
        if (!fileFolder.exists())
            fileFolder.mkdir();
        int start = urlPath.lastIndexOf("/") + 1;
        int end = urlPath.length();
        File file = new File ( folder + urlPath.substring(start , urlPath.length()));
        try {
            if (!file.exists())
                file.createNewFile();
            System.out.print("File Created.");

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        System.out.println("Fichero creado: " + file.getPath());
        return file;
    }

    public static boolean compareImage(File fileImageA, File fileImageB) throws IOException {
        BufferedImage biA;
        BufferedImage biB;
        if(fileImageA.getName().endsWith(".svg") || fileImageB.getName().endsWith(".svg")){
            System.out.println("Image A: " + fileImageA.getName());
            System.out.println("Image B: " + fileImageB.getName());
            if (!fileImageA.getName().endsWith(".svg") && fileImageB.getName().endsWith(".svg")){
                System.out.println("The images don't have the same extension");
                return false;
            }
            System.out.println("We have an .svg file.");
            biA = null;
            try {
                return svgCompare(fileImageA, fileImageB);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }else {
            biA = ImageIO.read(fileImageA);
            biB = ImageIO.read(fileImageB);
            DataBuffer dbA = biA.getData().getDataBuffer();
            int sizeA = dbA.getSize();
            DataBuffer dbB = biB.getData().getDataBuffer();
            int sizeB = dbB.getSize();

            int widthA = biA.getWidth();
            int widthB = biB.getWidth();
            int heightA = biA.getHeight();
            int heightB = biB.getHeight();

            //Compare images
            System.out.println("Start comparing: " + fileImageA + " with: " + fileImageB);
            System.out.println("The size of fileA is:" + sizeA);
            System.out.println("The size of fileB is:" + sizeB);
            System.out.println("The width of the image A is: " + widthA);
            System.out.println("The width of the image B is: " + widthB);
            System.out.println("The height of the image A is: " + heightA);
            System.out.println("The height of the image B is: " + heightB);
            if (widthA != widthB || heightA != heightB){
                System.out.println("The image dimensions are not the same.");
                return false;
            }
            if (sizeA == sizeB){
                for (int i = 0; i < sizeA; i ++){
                    if (dbA.getElem(i) != dbB.getElem(i)){
                        System.out.println("The images are not equals.");
                        return false;
                    }
                }
            }else {
                System.out.println("The size of the Image A, is not the same than the image B.");
                return false;
            }
            System.out.println("Great the images are equals.");
            return true;
        }

        return false;
    }

    public static boolean svgCompare(File svgFileA, File svgFileB ) throws Exception {
        FileReader frA = new FileReader(svgFileA);
        FileReader frB = new FileReader(svgFileB);
        BufferedReader brA = new BufferedReader(frA);
        BufferedReader brB = new BufferedReader(frB);
        long siceA = svgFileA.getTotalSpace();
        long siceB = svgFileB.getTotalSpace();
        boolean workFine = true;
        if (siceA != siceB){
            System.out.println("The size og the SVG are not the same, so they SVG are differents.");
            workFine =  false;
        }
        String lineA;
        String lineB;

        while ((lineA = brA.readLine()) != null) {
            lineB = brB.readLine();
            if (!lineA.equals(lineB)){
                System.out.println("ERROR: lineA: \n" + lineA + "\n is not equals than lineB: \n" + lineB);
                workFine =  false;
            }
        }
        frA.close();
        frB.close();
        System.out.println("The SVG images are equals.");
        return workFine;
    }

}
