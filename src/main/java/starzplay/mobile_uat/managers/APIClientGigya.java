package starzplay.mobile_uat.managers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.Test;
import starzplay.mobile_uat.managers.testrail.APIException;
import starzplay.mobile_uat.resources.Operators.Operator;
import starzplay.mobile_uat.resources.Operators.OperatorFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


public class APIClientGigya {

	private static String m_url = "https://accounts.eu1.gigya.com/";
	private static final String API_KEY = "3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl";
	private static final String API_KEY_Prod = "3_dFt8EnQzDmeB6DwcuF2lf2E6JIsbjiwSZolwwwCw58kBDmouvah9cyMxFxOOEtAl";
	private static final String SECRET_KEY = "rRiE4LPpZ8FJZzWoBUsPFAH5TF2vo6Vjdt5PtULWR/c";
	static String uid;
	static String phone;

	public static Object sendGet(String uri)
			throws IOException, APIException
	{
		return APIClientGigya.sendRequest("GET", uri, null);
	}


	public static Object sendPost(String uri, Object data)
			throws IOException, APIException
	{
		return APIClientGigya.sendRequest("POST", uri, data);
	}

	private static Object sendRequest(String method, String uri, Object data)
			throws IOException, APIException
	{
		URL url = new URL(APIClientGigya.m_url + uri);

		HttpURLConnection conn = (HttpURLConnection)url.openConnection();
		System.out.println("conn " + conn );

		if (method.equals("POST"))
		{
			// Add the POST arguments, if any. We just serialize the passed
			// data object (i.e. a dictionary) and then add it to the
			// request body.
			if (data != null)
			{
				byte[] block = JSONValue.toJSONString(data).
						getBytes("UTF-8");

				conn.setDoOutput(true);
				OutputStream ostream = conn.getOutputStream();
				ostream.write(block);
				ostream.flush();
			}
		}

		// Execute the actual web request (if it wasn't already initiated
		// by getOutputStream above) and record any occurred errors (we use
		// the error stream in this case).
		int status = conn.getResponseCode();
		System.out.println(status);
		InputStream istream;
		if (status != 200)
		{
			istream = conn.getErrorStream();
			if (istream == null)
			{
				throw new APIException(
						"API return HTTP " + status +
								" (No additional error message received)"
				);
			}
		}
		else
		{
			istream = conn.getInputStream();
		}

		// Read the response body, if any, and deserialize it from JSON.
		String text = "";
		if (istream != null)
		{
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(
							istream,
							"UTF-8"
					)
			);

			String line;
			while ((line = reader.readLine()) != null)
			{
				text += line;
				text += System.getProperty("line.separator");
			}

			reader.close();
		}

		Object result;
		if (!text.equals(""))
		{
			result = JSONValue.parse(text);
		}
		else
		{
			result = new JSONObject();
		}

		// Check for any occurred errors and add additional details to
		// the exception message, if any (e.g. the error message returned
		// by TestRail).
		if (status != 200)
		{
			String error = "No additional error message received";
			if (result != null && result instanceof JSONObject)
			{
				JSONObject obj = (JSONObject) result;
				if (obj.containsKey("error"))
				{
					error = '"' + (String) obj.get("error") + '"';
				}
			}

			throw new APIException(
					"API returned HTTP " + status +
							"(" + error + ")"
			);
		}

		return result;
	}

	private static String getUserIDBySearchAccount(String field, String value) throws Exception{

		JSONObject c = (JSONObject)sendGet("accounts.search?query=select%20UID%20from%20accounts%20where%20" +
				field +
				"=%27" +
				value +
				"%27&secret=" + SECRET_KEY + "=&format=json&apiKey=" + API_KEY);
		JSONArray as = (JSONArray)c.get("results");
		if (as.size()==0)
			System.out.println("No Gigya user found with that search");
		else {
			for (Object a : as) {
				JSONObject run = (JSONObject) a;
				uid = (String) run.get("UID");
				System.out.println("uid = " + uid);
			}
		}

		return uid;
	}

	public static void deleteGigyaData(Operator partner) throws Exception {

		phone = partner.getCountryPrefix() + partner.getValidTelephoneNumber();
		String billingPhoneID = getUserIDBySearchAccount("billingPhone", phone);
		String userNameID = getUserIDBySearchAccount("loginIDs.username", phone);
		try {
			if (billingPhoneID != null)
				sendPost(("accounts.setAccountInfo?format=json&UID=" + billingPhoneID +
						"&apiKey=" + API_KEY + "&secret=" + SECRET_KEY + "=&data=%7B%22billingPhone%22:%22%22%7D"), null);
			if (userNameID != null)
				sendPost(("accounts.setAccountInfo?format=json&UID=" + userNameID +
						"&apiKey=" + API_KEY + "&secret=" + SECRET_KEY + "=&username=" + System.currentTimeMillis()), null);
		} catch (Exception e) {
			System.out.println("Exception updating Gigya user.");
			System.out.println(e);
		}


	}

	public static void deleteUserData(Operator partner) throws Exception {

		phone = partner.getCountryPrefix() + partner.getValidTelephoneNumber();
		String billingPhoneID = getUserIDBySearchAccount("billingPhone", phone);
		String userNameID = getUserIDBySearchAccount("loginIDs.username", phone);
		DBConnectionManager.updateQuery("update `mobile_users` set `mobile_number` = '" + System.currentTimeMillis() + "' where `mobile_number` = '" + phone + "'");

		try {
			if (billingPhoneID != null)
				sendPost(("accounts.setAccountInfo?format=json&UID=" + billingPhoneID +
						"&apiKey=" + API_KEY + "&secret=" + SECRET_KEY + "=&data=%7B%22billingPhone%22:%22%22%7D"), null);
			if (userNameID != null)
				sendPost(("accounts.setAccountInfo?format=json&UID=" + userNameID +
						"&apiKey=" + API_KEY + "&secret=" + SECRET_KEY + "=&username=" + System.currentTimeMillis()), null);
		} catch (Exception e) {
			System.out.println("Exception updating Gigya user.");
			System.out.println(e);
		}

		Thread.sleep(5000);

	}

	public static void deleteUserData(String partner,String period) throws Exception {

		Operator operator = OperatorFactory.getOperator(partner);
		phone = operator.getCountryPrefix() + operator.getValidTelephoneNumber();
		String billingPeriod = operator.getPaymentName();
		String billingPhoneID = getUserIDBySearchAccount("billingPhone", phone);
		String userNameID = getUserIDBySearchAccount("loginIDs.username", phone);

		DBConnectionManager.updateQuery("update `mobile_users` set `mobile_number` = '" + System.currentTimeMillis() + "' where `mobile_number` = '" + phone + "'");

//		if (period.equalsIgnoreCase("week"))
//			billingPeriod = billingPeriod + "_week";

		try {
			if (billingPhoneID != null)
				sendPost(("accounts.setAccountInfo?format=json&UID=" + billingPhoneID +
						"&apiKey=" + API_KEY + "&secret=" + SECRET_KEY + "=&data=%7B%22billingPhone%22:%22%22%7D"), null);
			if (userNameID != null)
				sendPost(("accounts.setAccountInfo?format=json&UID=" + userNameID +
						"&apiKey=" + API_KEY + "&secret=" + SECRET_KEY + "=&username=" + System.currentTimeMillis()), null);

		} catch (Exception e) {
			System.out.println("Exception updating Gigya user.");
			System.out.println(e);
		}


		if (partner.equalsIgnoreCase("vodafone") || partner.equalsIgnoreCase("stc") || partner.equalsIgnoreCase("etisalat")) {
			try {

				String body = "{\n" +
						"\t\"msisdn\":\"" + phone + "\", \n" +
						"\t\"mop\":\"" + billingPeriod + "_week" + "\",\n" +
						"\t\"spx_trx_id\":\"" + "35678" + "\", \n" +
						"\t\"description\":\"" + "Unsubscribe request from user" + "\"\n" +
						"}";
				APIRequestManager.sendPutMSISDN("http://tst-spx-pg.eu.cloudhub.io/api/internalRequest/user/unsubscribe", body);
			} catch (Exception e) {
				System.out.println("Exception unsusbcribe weekly user from tst");
				System.out.println(e);
			}
			try {

				String body = "{\n" +
						"\t\"msisdn\":\"" + phone + "\", \n" +
						"\t\"mop\":\"" + billingPeriod + "\",\n" +
						"\t\"spx_trx_id\":\"" + "35678" + "\", \n" +
						"\t\"description\":\"" + "Unsubscribe request from user" + "\"\n" +
						"}";
				APIRequestManager.sendPutMSISDN("http://tst-spx-pg.eu.cloudhub.io/api/internalRequest/user/unsubscribe", body);
			} catch (Exception e) {
				System.out.println("Exception unsusbcribe monthly user from tst.");
				System.out.println(e);
			}
			try {

				String body = "{\n" +
						"\t\"msisdn\":\"" + phone + "\", \n" +
						"\t\"mop\":\"" + billingPeriod + "_week" + "\",\n" +
						"\t\"spx_trx_id\":\"" + "35678" + "\", \n" +
						"\t\"description\":\"" + "Unsubscribe request from user" + "\"\n" +
						"}";
				APIRequestManager.sendPutMSISDN("http://prd-spx-pg.eu.cloudhub.io/api/internalRequest/user/unsubscribe", body);
			} catch (Exception e) {
				System.out.println("Exception unsusbcribe weekly user from prd.");
				System.out.println(e);
			}
			try {

				String body = "{\n" +
						"\t\"msisdn\":\"" + phone + "\", \n" +
						"\t\"mop\":\"" + billingPeriod + "\",\n" +
						"\t\"spx_trx_id\":\"" + "35678" + "\", \n" +
						"\t\"description\":\"" + "Unsubscribe request from user" + "\"\n" +
						"}";
				APIRequestManager.sendPutMSISDN("http://prd-spx-pg.eu.cloudhub.io/api/internalRequest/user/unsubscribe", body);
			} catch (Exception e) {
				System.out.println("Exception unsusbcribe monthly user from prd.");
				System.out.println(e);
			}

		}
	}

	@Test
	public void delete() throws Exception {
		deleteUserData("stc","week");
	}
}
