package starzplay.mobile_uat.managers;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;
import starzplay.mobile_uat.resources.Dictionary.HtmlSanitizer;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


/**
 * Created by mikelfernandez on 19/5/16.
 */

public class TranslationLanguagesManager {

    private String PROJECT_ID = "starzplay-arabia-portal";
    private String API_KEY = "9f172558d746ecfc6d528c71278bb9af";
    private String pack = "all";
    private String currentBranch = "a";
    private String urlExportTranslations = "https://api.crowdin.com/api/project/" +PROJECT_ID+ "/export?key=" + API_KEY + "&branch=" + currentBranch + "&json=true";
    private String urlDownloadTranslations = "https://api.crowdin.com/api/project/" +PROJECT_ID+ "/download/"+ pack+ ".zip?key="+API_KEY + "&branch=" + currentBranch + "&json=true'";

    @Test
    public void download_zip_file() throws Exception {
        APIRequestManager.sendGet(urlExportTranslations);

        URL url = new URL(urlDownloadTranslations);
        HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

// Check for errors
        int responseCode = con.getResponseCode();
        InputStream inputStream;
        if (responseCode == HttpURLConnection.HTTP_OK) {
            inputStream = con.getInputStream();
        } else {
            inputStream = con.getErrorStream();
        }

        FileOutputStream out = new FileOutputStream("src/test/resources/translation/test.zip");

        byte[] b = new byte[1024];
        int count;
        while ((count = inputStream.read(b)) >= 0) {
            out.write(b, 0, count);
        }
        out.flush();
        out.close();
        inputStream.close();

        unZip();

    }

    public static void unZip() {

        try {
            ZipFile zipFile = new ZipFile("src/test/resources/translation/test.zip");
            Enumeration<?> enu = zipFile.entries();
            while (enu.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry) enu.nextElement();

                String name = zipEntry.getName();
                long size = zipEntry.getSize();
                long compressedSize = zipEntry.getCompressedSize();
                System.out.printf("name: %-20s | size: %6d | compressed size: %6d\n",
                        name, size, compressedSize);

                File file = new File("src/test/resources/translation/" + name);
                if (name.endsWith("/")) {
                    file.mkdirs();
                    continue;
                }

                File parent = file.getParentFile();
                if (parent != null) {
                    parent.mkdirs();
                }

                InputStream is = zipFile.getInputStream(zipEntry);
                FileOutputStream fos = new FileOutputStream(file);
                byte[] bytes = new byte[1024];
                int length;
                while ((length = is.read(bytes)) >= 0) {
                    fos.write(bytes, 0, length);
                }
                is.close();
                fos.close();

            }
            zipFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getTranslation(String language, String folder,String file, String text, Boolean ... parseHtml) {
        JSONParser parser = new JSONParser();
        String name ="";
        try {
            Object obj = parser.parse(new FileReader("src/test/resources/translation/" + language + "/" + folder + "/"+  file + ".json"));
            JSONObject jsonObject = (JSONObject) obj;
            name = (String) jsonObject.get(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if( parseHtml.length > 0 ) {
            return HtmlSanitizer.sanitize(name);
        }
        return name;
    }


}
