package starzplay.mobile_uat.managers.testrail;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class APITestRailManager {

    static String testRailUserName = "q.a@starzplayarabia.com";
    static String testRailPassword = "Test123@";
    static String testRailEndPoint = "https://starzplay.testrail.net/";
    APIClient client;
    Map data;
    int status_id;
    JSONObject result;
    String project_id;
    String runID;
    String caseID;
    String projectID;
    String sectionID;
    public static List steps;

    public APIClient getTestRailClient(){
        client = new APIClient(testRailEndPoint);
        client.setUser(testRailUserName);
        client.setPassword(testRailPassword);

        return client;
    }

    public void updateTestrail(String caseId, String runId, boolean testResult, String msg) {
        client = getTestRailClient();

        //status_id is 1 for Passed, 2 For Blocked, 4 for Retest and 5 for Failed
        if (testResult==true)
            status_id = 1;
        else
            status_id = 5;

        try {
            data = new HashMap();
            data.put("custom_testcase_status", 1);
            data.put("custom_step_results", steps);
            data.put("status_id", status_id);
            data.put("comment", msg);
            result = (JSONObject)client.sendPost(("add_result_for_case/" + runId + "/" + caseId), data);
        } catch (Exception e) {
            System.out.println("Exception in updateTestrail() updating TestRail.");
            System.out.println(e);
        }

        System.out.println("Updated test result for case: " + caseId + " in test run: " + runId + " with msg: " + msg);

    }

    public void  updateTestCase(String caseName, String projectName)throws Exception {
        client = getTestRailClient();
        caseID = getTestCaseID(caseName,projectName);

        try {
            data = new HashMap();
            data.put("template_id", "3");
            data.put("custom_steps_separated", steps);
            result = (JSONObject)client.sendPost(("update_case/"+ caseID), data);
        } catch (Exception e) {
            System.out.println("Exception in updateTestrail() updating TestRail.");
            System.out.println(e);
        }

    }


    public String getProjectId(String projectName) throws Exception {
        client = getTestRailClient();

        JSONArray projects = (JSONArray)client.sendGet("get_projects");
        for (int i = 0; i < projects.size(); i++) {
            JSONObject project = (JSONObject) projects.get(i);
            if (project.get("name").equals(projectName)) {
                Long s = (Long) project.get("id");
                projectID = String.valueOf(s);
                break;
            }
        }

        return projectID;
    }



    public String  getRunId(String testRunName, String projectName)throws Exception {
        client = getTestRailClient();
        project_id = getProjectId(projectName);

        JSONArray testRuns = (JSONArray) client.sendGet("get_runs/" + project_id);
        for (int i = 0; i < testRuns.size(); i++) {
            JSONObject run = (JSONObject) testRuns.get(i);
            if (run.get("name").equals(testRunName)) {
                Long s = (Long) run.get("id");
                runID = String.valueOf(s);
                break;
            }
        }

//        for (String testRun : testRuns) {
//            if (testRun.get("name").equals(testRunName)) {
//                runID = testRun.get("id");
//                break;
//            }
//        }
        return runID;

    }

    public JSONArray getRuns(String projectName) throws Exception{
        client = getTestRailClient();

        JSONArray testRuns = (JSONArray) client.sendGet("get_runs/" + getProjectId(projectName));
        for (int i = 0; i < testRuns.size(); i++) {
            JSONObject run = (JSONObject) testRuns.get(i);
            System.out.println(run.get("name") + " " + run.get("id"));
        }

        return testRuns;
    }

    public JSONObject addRun(String projectName) throws Exception{
        client = getTestRailClient();
        data = new HashMap();
        data.put("name", "testrunnnn");
        data.put("description", "testrun creado desde codigo");

        JSONObject newRun = (JSONObject)client.sendPost("add_run/" +getProjectId(projectName) ,data);

        return newRun;

    }

    public void getUsers() throws Exception{
        client = getTestRailClient();
        JSONArray c = (JSONArray) client.sendGet("get_users");
    }

    public void getUser() throws Exception{
        client = getTestRailClient();
        JSONObject c = (JSONObject) client.sendGet("get_user/1");
    }


    public String getTestCaseID(String testCaseName, String projectName)throws Exception {
        client = getTestRailClient();
        project_id = getProjectId(projectName);

        JSONArray testCases = (JSONArray) client.sendGet("get_cases/" + project_id);
        for (int i = 0; i < testCases.size(); i++) {
            JSONObject run = (JSONObject) testCases.get(i);
            if (run.get("title").equals(testCaseName)) {
                Long s = (Long) run.get("id");
                caseID = String.valueOf(s);
                break;
            }
        }

        return caseID;

    }



    public void  updateTestCase2(String caseName, String projectName)throws Exception {
        client = getTestRailClient();
        caseID = getTestCaseID(caseName,projectName);
        if (caseID == null){
            addTestCase("",caseName);
        }

        try {
            data = new HashMap();
            data.put("custom_steps_separated", steps);
            result = (JSONObject)client.sendPost(("update_case/"+ caseID), data);
        } catch (Exception e) {
            System.out.println("Exception in updateTestrail() updating TestRail.");
            System.out.println(e);
        }

    }

    public void  addTestCase(String sectionId, String caseName)throws Exception {
        client = getTestRailClient();
        try {
            data = new HashMap();
            data.put("title", caseName);
            result = (JSONObject)client.sendPost(("add_case/"+ sectionID), data);
        } catch (Exception e) {
            System.out.println("Exception in updateTestrail() updating TestRail.");
            System.out.println(e);
        }

    }

    public void getRuns() throws Exception{
        client = getTestRailClient();
        JSONArray c = (JSONArray) client.sendGet("get_runs/1");
    }

    public void getRunsProject(String projectName) throws Exception{
        client = getTestRailClient();

        JSONArray testRuns = (JSONArray) client.sendGet("get_runs/" + getProjectId(projectName));
        for (int i = 0; i < testRuns.size(); i++) {
            JSONObject run = (JSONObject) testRuns.get(i);
            System.out.println(run.get("name") + " " + run.get("id"));
        }
    }

    public void getRun() throws Exception{
        client = getTestRailClient();
        JSONObject c = (JSONObject) client.sendGet("get_run/1");
    }

    public void getSections(String projectName) throws Exception{
        client = getTestRailClient();
        project_id = getProjectId(projectName);

        JSONArray testSections = (JSONArray) client.sendGet("get_sections/" + project_id);
        for (int i = 0; i < testSections.size(); i++) {
            JSONObject run = (JSONObject) testSections.get(i);
            System.out.println("name " + run.get("name"));
            System.out.println(run.get("depth"));
            System.out.println(run.get("id"));
            System.out.println(run.get("suite_id"));
            System.out.println(run.get("parent_id"));
        }
    }

    public void getTemplates(String projectName) throws Exception{
        client = getTestRailClient();
        project_id = getProjectId(projectName);

        JSONArray testSections = (JSONArray) client.sendGet("get_templates/" + project_id);
        for (int i = 0; i < testSections.size(); i++) {
            JSONObject run = (JSONObject) testSections.get(i);
        }

//        JSONArray c = (JSONArray) client.sendGet("get_templates/" + project_id);
//        System.out.println(c.get(3));
    }


}
